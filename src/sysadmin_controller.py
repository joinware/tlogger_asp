# -*- coding: utf-8 -*-
import os
from google.appengine.ext import webapp
from google.appengine.api import users
import logging
import time

#LOCAL
from paging import *
from posmodel import *
webapp.template.register_template_library('filters.filter')
#from intro_model import *
from ajax_controller import *


#DiretoryInfo
dir_name = "sysadmin"
slashdir_name = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"


###################################################
# Main Module
###################################################
class UsageTip(Helper):
    def get(self):
        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':"TIP",
            }
        self.helpDispatch(dir_name, 'tip.html', template_vals)

class CompanyList(Helper):
    def get(self):
        sel_todouhuken   = self.request.get('sel_todouhuken')

        items = []
        if sel_todouhuken:
            if sel_todouhuken == "00":
                items = CompanyInfo().all()
            else:
                items  = CompanyInfo.all().filter("company_id >", sel_todouhuken + "0000").filter("company_id <=", sel_todouhuken + "9999")
        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':"会社リスト", #Company List",
                'items': items,
                'sel_todouhuken':sel_todouhuken,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:NoCalendar, -9:NoConditionBar
            }
        self.helpDispatch(dir_name, 'complist.html', template_vals)

'''
신규회사 등록
1) 첫번째 점포도 자동등록함.
'''
class NewCompanyEdit(Helper):
    def get(self):
        #loginData = self.logincheck()
        title = ""
        company_id = self.request.get('company_id')
        if company_id: #UPDATE
            obj_comp = CompanyInfo.get_by_key_name(company_id)
            if not obj_comp:
                logging.error('Shopman_Controller::ShopEdit:: not found company=' + company_id)
                return

            if self.request.get('delete'):
                db.delete( obj_comp.grouptable_set )
                db.delete( obj_comp.siiretable_set )
                #for item in obj_comp.shopinfo_set:
                #    func_shop_delete(item)

                '''
                #その他の関連テーブルの削除
                '''
                obj_comp.delete()
                self.redirect(slashdir_name)
                return
            kubun = "update"
            title = "会社情報修正"
        else: #INSERT
            obj_comp = CompanyInfo()
            kubun = "insert"
            title = "新規会社作成"

        #lang_sel = loginData['shop_user'].lang_sel
        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':title,
                "item":obj_comp,
                "kubun":kubun,
            }
        self.helpDispatch('sysadmin', 'companyedit.html', template_vals)

    def post(self):
        try:
            kubun      = self.request.get('kubun')
            company_id = self.request.get('company_id')
            kname      = self.request.get('kname')
            zipcode    = self.request.get('zipcode')
            address1   = self.request.get('address1')
            address2   = self.request.get('address2')
            telno      = self.request.get('telno')
            admin_id   = self.request.get('admin_id')
            agency_no  = int(self.request.get('agency_no'))
            if kubun == "update":
                obj_comp = get_mem_company(company_id)
                if not obj_comp:
                    logging.error('sysadmin_controller::NewCompanyEdit:: not found company=' + company_id)
                    return
                obj_comp.kname     = kname
                obj_comp.zipcode   = zipcode
                obj_comp.address1  = address1
                obj_comp.address2  = address2
                obj_comp.telno     = telno
                obj_comp.agency_no = agency_no
                #logging.error('sysadmin_controller::NewCompanyEdit:: group_names=' + group_names)
                #logging.error('sysadmin_controller::NewCompanyEdit:: group_1_nos=' + group_1_nos)

                #アカウント変更
                if admin_id != obj_comp.admin_id:
                    result = self.account_check_save(obj_comp, admin_id)
                    if not result:
                        raise UserException(u"Already used ID=%s"%admin_id)
                #
                obj_comp.admin_id = admin_id
                obj_comp.put()
                self.redirect(slashdir_name)
                return

            #アカウントチェック
            if admin_id:
                result = self.account_check_save(None, admin_id)
                if not result:
                    raise UserException(u"Already used ID=%s"%admin_id)
            #
            obj = CompanyInfo().insert_or_fail(
                                company_id  = company_id,
                                kname     = kname,    #ShopName
                                zipcode   = zipcode,
                                address1  = address1, #住所１
                                address2  = address2, #住所２
                                telno     = telno,    #電話番号
                                agency_no = agency_no,
                                admin_id  = admin_id
                              )

            if not obj:
                self.redirect(slashdir_name)
                return

            obj_shop = ShopInfo().insert_or_fail(
                                company_key  = obj,
                                company_id   = obj.company_id,
                                kname        = kname,
                                zipcode      = zipcode,
                                address1     = address1, #住所１
                                address2     = address2, #住所２
                                telno        = telno,    #電話番号
                                admin_id     = admin_id
                              )
            #会社管理者
            if admin_id:
                ShopUser().insert_or_fail(
                                company_key = obj,
                                email_id    = admin_id,
                                company_id  = company_id,
                                shop_id     = obj_shop.shop_id,
                                name        = "RootAdmin",
                                role_flag   = 1  #대부분 점포1개가 대부분이므로 회사관리자ID는 필요없음.
                              )
            self.redirect(slashdir_name)
        except UserException as e:
            occur_point = "%s::NewCompanyEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::NewCompanyEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise

    '''
    '''
    def account_check_save(self, obj_comp, new_account):
        new_user = ShopUser.get_by_key_name(new_account)
        if new_user:
            logging.debug('shopman_controller::ShopEdit:: used account shop=%s-%s acount=%s'%(new_user.company_id, new_user.shop_id, new_account))
            return False
        if obj_comp:
            old_user = ShopUser.get_by_key_name(obj_comp.admin_id)
            if old_user: old_user.delete()
            #ShopUser追加
            ShopUser().insert_or_fail(
                            company_key = obj_comp,
                            shop_id     = obj_comp.company_id + "01",
                            email_id    = new_account,
                            role_flag   = 9
                          )
        return True




'''
店舗マスタ
'''
class ShopList(Helper):
    def get(self):
        #loginData = self.logincheck()
        company_id = self.request.get('company_id')
        data = get_mem_company(company_id)
        if not data: return
        #shops = data.shopinfo_set
        shops = ShopInfo.all().filter("shop_id >", company_id + "00" ).filter("shop_id <", company_id + "99" )
        shop_id_list = []
        for item in shops:
            shop_id_list.append(item.shop_id)
        userlist = ShopUser.all().filter("shop_id IN", shop_id_list ).order('shop_id')

        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':"店舗リスト", #Shop List",
                'items': shops,
                'company_id': company_id,
                'company_name': data.kname,
                'userlist': userlist,
            }
        self.helpDispatch(dir_name, 'shoplist.html', template_vals)


    def post(self):
        #loginData = self.logincheck()
        #if not loginData: return

        company_id = self.request.get('company_id')

        it_shop_option = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        it_shop_option[0] = int(self.request.get('siire_option'))
        it_shop_option[1] = int(self.request.get('labor_option'))
        it_shop_option[2] = int(self.request.get('group_option'))
        it_shop_option[3] = int(self.request.get('table_option'))

        asp_period     = int(self.request.get('asp_period'))
        asp_last_month = self.request.get('asp_last_month')
        asp_hurikomi   = int(self.request.get('asp_hurikomi'))
        customer_url   = self.request.get('customer_url')
        pdf_name = self.request.get('pdf_name') #請求書のURL

        #ASP契約
        shop_list = []
        shop_id_list = self.request.get_all('shop_id')
        for shop_id in shop_id_list:
            #tmp = [shop_id, 0, 0, 0, 0, False, 0, 0, 0, 0, 0, 0, False, 0, "", "", "", 0, False, 0, 0, 0, 0, 0, 0, 0,0,0,0,0, 0] #0 ~ 30
            tmp = [0  for j in range(50)]
            tmp[0]  = shop_id
            tmp[5]  = False
            tmp[12] = False
            tmp[14] = ""
            tmp[15] = ""
            tmp[16] = ""
            tmp[18] = False
            #tmp[19] = 5000 #ASP費用
            shop_list.append(tmp)
        shop_len = len(shop_list)

        asp1s  = self.request.get_all('asp_1')
        asp2s  = self.request.get_all('asp_2')
        asp3s  = self.request.get_all('asp_3')
        asp4s  = self.request.get_all('asp_4')  #TabOption表示順⇒IDに変更(あとで削除すること)
        asp5s  = self.request.get_all('asp_5')  #WEB注文　
        asp6s  = self.request.get_all('asp_6')  #Point
        asp7s  = self.request.get_all('asp_7')  #セルフ端末
        asp71s = self.request.get_all('asp_71') #セルフ端末飲み放題
        asp8s  = self.request.get_all('asp_8')
        asp9s  = self.request.get_all('asp_9')
        asp10s = self.request.get_all('asp_10')
        asp11s = self.request.get_all('asp_11')
        asp12s = self.request.get_all('asp_12')
        asp13s = self.request.get_all('asp_13') #点検レポート
        asp14s = self.request.get_all('asp_14') #店舗日報
        asp15s = self.request.get_all('asp_15') #サブレジ有
        asp16s = self.request.get_all('asp_16') #Point Pay
        asp17s = self.request.get_all('asp_17') #商品バーコード使用
        asp18s = self.request.get_all('asp_18') #券売機
        asp19s = self.request.get_all('asp_19') #厨房用言語
        #
        for shop_id in asp1s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][1] = 1
                    break
        for shop_id in asp2s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][2] = 1
                    break
        for shop_id in asp3s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][3] = 1
                    break
        for shop_id in asp4s:  #TabOption表示順⇒IDに変更(あとで削除すること)
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][4] = 1
                    break
        for shop_id in asp5s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][8] = 1
                    break
        for shop_id in asp6s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][9] = 1
                    break
        for shop_id in asp7s: #SelfOrder
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][13] = 1
                    break
        for shop_id in asp71s: #SelfOrder No Unlimited
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][17] = 1
                    break
        for shop_id in asp8s: #Table Layout
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][25] = 1
                    break
        for shop_id in asp9s: #Table Layout
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][26] = 1
                    break
        for shop_id in asp10s: #Table Layout
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][27] = 1
                    break
        for shop_id in asp11s: #Table Layout
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][28] = 1
                    break
        for shop_id in asp12s: #Table Layout
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][29] = 1
                    break
        for shop_id in asp13s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][31] = 1
                    break
        for shop_id in asp14s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][32] = 1
                    break
        for shop_id in asp15s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][33] = 1
                    break
        for shop_id in asp16s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][34] = 1
                    break
        for shop_id in asp17s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][35] = 1
                    break
        for shop_id in asp18s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][36] = 1
                    break
        for shop_id in asp19s:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][37] = 1
                    break
        #-----
        printer_image_files  = self.request.get_all('printer_image_file')
        for shop_id in printer_image_files:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][18] = True
                    break
        ticket_grouping_drinks = self.request.get_all('grouping_drink')
        for shop_id in ticket_grouping_drinks:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][19] = 1
                    break
        ticket_grouping_foods  = self.request.get_all('grouping_food')
        for shop_id in ticket_grouping_foods:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][20] = 1
                    break
        ticket_grouping_all  = self.request.get_all('drink_print_all')
        for shop_id in ticket_grouping_all:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][30] = 1
                    break

        month_cost_list = self.request.get_all('month_cost') #ASP費用
        for i, cost in enumerate(month_cost_list):
            shop_list[i][40] = int(cost)
        '''
        backups    = self.request.get_all('backup')
        for shop_id in backups:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][5] = True
                    break
        '''
        print_cnts = self.request.get_all('print_cnt')
        for i, cnt in enumerate(print_cnts):
            shop_list[i][6] = int(cnt)

        handy_cnts = self.request.get_all('handy_cnt')
        for i, cnt in enumerate(handy_cnts):
            shop_list[i][7] = int(cnt)

        print_model = self.request.get_all('print_model')
        for i, cnt in enumerate(print_model):
            shop_list[i][10] = int(cnt)

        upper_blank_lines = self.request.get_all('upper_blank_line')
        for i, cnt in enumerate(upper_blank_lines):
            shop_list[i][38] = int(cnt)
        lower_blank_lines = self.request.get_all('lower_blank_line')
        for i, cnt in enumerate(lower_blank_lines):
            shop_list[i][39] = int(cnt)

        audits = self.request.get_all('audit')
        for shop_id in audits:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][11] = 1
                    break
        up_stops = self.request.get_all('up_stop')
        for shop_id in up_stops:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][12] = True
                    break

        multi_lang1s = self.request.get_all('multi_lang1')
        multi_lang2s = self.request.get_all('multi_lang2')
        multi_lang3s = self.request.get_all('multi_lang3')
        for i, str_language in enumerate(multi_lang1s):
            shop_list[i][14] = str_language
        for i, str_language in enumerate(multi_lang2s):
            shop_list[i][15] = str_language
        for i, str_language in enumerate(multi_lang3s):
            shop_list[i][16] = str_language

        #
        table_status_option0 = self.request.get_all('table_status_option0')
        table_status_option1 = self.request.get_all('table_status_option1')
        table_status_option2 = self.request.get_all('table_status_option2')
        table_status_option3 = self.request.get_all('table_status_option3')
        for shop_id in table_status_option0:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][21] = 1
                    break
        for shop_id in table_status_option1:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][22] = 1
                    break
        for shop_id in table_status_option2:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][23] = 1
                    break
        for shop_id in table_status_option3:
            for k in range(shop_len):
                if shop_list[k][0] == shop_id:
                    shop_list[k][24] = 1
                    break

        for k in range(shop_len):
            shop_key = shop_list[k][0]
            obj = ShopInfo.get_by_key_name(shop_key)
            if not obj:
                logging.error('ShopEdit:: not found shop_key=' + shop_key)
                return
            #logging.error("shop_id=%s, %d, %d, %d, %d"%(shop_list[k][0], shop_list[k][1], shop_list[k][2], shop_list[k][3], shop_list[k][4]))
            #list_setup = [0,0,0,0,0,0,0,0,0,0,0,0,0]
            list_setup    = [0  for j in range(20)]
            list_setup[0] = shop_list[k][1]
            list_setup[1] = shop_list[k][2]
            list_setup[2] = shop_list[k][3]
            list_setup[3] = shop_list[k][4] #TabOption表示順⇒IDに変更(あとで削除すること)
            #
            list_setup[4] = shop_list[k][8]   #WEB注文　
            list_setup[5] = shop_list[k][9]
            list_setup[6] = shop_list[k][13]  #セルフ端末
            list_setup[7] = shop_list[k][17]  #セルフ端末飲み放題
            list_setup[8] = shop_list[k][25]  #Table Layout
            list_setup[9] = shop_list[k][26]  #月間FLの売上を税抜きにする。
            list_setup[10] = shop_list[k][27] #顧客数入力スキップ
            list_setup[11] = shop_list[k][28] #在庫維持
            list_setup[12] = shop_list[k][29] #全てのレポートを税抜きにする。
            list_setup[13] = shop_list[k][31] #点検レポート
            list_setup[14] = shop_list[k][32] #店舗日報
            list_setup[15] = shop_list[k][33] #Sub Regi
            list_setup[16] = shop_list[k][34] #PointPay
            list_setup[17] = shop_list[k][35] #領収書
            list_setup[18] = shop_list[k][36] #券売機
            list_setup[19] = shop_list[k][37] #厨房用言語
            obj.it_setup  = list_setup
            #
            obj.sdcard_backup = shop_list[k][5]
            obj.print_cnt     = shop_list[k][6]
            obj.handy_cnt     = shop_list[k][7]
            obj.printer_model = shop_list[k][10]
            obj.audit_kbn     = shop_list[k][11]
            obj.up_stop       = shop_list[k][12]
            obj.st_multi_lang = [shop_list[k][14], shop_list[k][15], shop_list[k][16]]
            obj.printer_image_file = shop_list[k][18]
            obj.print_ticket_grouping = [shop_list[k][19], shop_list[k][20], shop_list[k][30]]
            obj.ticket_upper_blank = shop_list[k][38]
            obj.ticket_lower_blank = shop_list[k][39]
            obj.asp_month_cost     = shop_list[k][40]

            #
            obj.it_shop_option = it_shop_option
            obj.asp_period  = asp_period
            if asp_last_month:
                obj.asp_last_month  = asp_last_month
            obj.asp_hurikomi  = asp_hurikomi

            obj.it_table_status =  [shop_list[k][21], shop_list[k][22], shop_list[k][23], shop_list[k][24] ]
            #
            obj.customer_url  = customer_url
            obj.address3 = pdf_name

            obj.put()
            del_mem_shop(shop_key)
            #
            time.sleep(1)
        #--
        self.redirect(slashdir_name + "shoplist?company_id=" + company_id)



'''
店舗管理者マスタ
'''
class ShopUserList(Helper):
    def get(self):
        #loginData = self.logincheck()
        data = None
        userlist = []
        company_id = self.request.get('company_id')
        user_id = self.request.get('user_id')
        if user_id:
            data = ShopUser.get_by_key_name(user_id)
            if data:
                userlist.append(data)
        else:
            data = get_mem_company(company_id)
            if not data: return
            #shops = data.shopinfo_set
            shops = ShopInfo.all().filter("shop_id >", company_id + "00" ).filter("shop_id <", company_id + "99" )
            shop_id_list = []
            for item in shops:
                shop_id_list.append(item.shop_id)
            userlist = ShopUser.all().filter("shop_id IN", shop_id_list ).order('shop_id')

        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':"店舗管理者リスト", #Shop List",
                'company_id': company_id,
                'company_data': data,
                'userlist': userlist,
                'user_id':user_id,
            }
        self.helpDispatch(dir_name, 'shopuserlist.html', template_vals)



    def post(self):
        company_id = self.request.get('company_id')
        account_id = self.request.get('account_id')
        shop_id    = self.request.get('new_shop_id')
        role_flag  = int(self.request.get('role_flag'))
        disabl_edit= int(self.request.get('disabl_edit'))
        regi_no    = int(self.request.get('regi_no'))
        regi_type  = int(self.request.get('regi_type'))
        group_no   = int(self.request.get('group_no'))

        obj_shop = ShopInfo.get_by_key_name(shop_id)
        if not obj_shop:
            logging.error('ShopUserList:: not found shop_id =%s'%(shop_id))
            return

        flag_tencyo = True
        if role_flag != 1: flag_tencyo = False
        obj_user = ShopUser.get_by_key_name(account_id)
        #
        if self.request.get('search_user'):
            user_id = int(self.request.get('user_id'))
            #logging.debug('ShopUserList:: user_id=%s'%(user_id))
            obj_user = ShopUser.get_by_key_name(user_id)
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return
        #
        if self.request.get('update_account'):
            logging.debug('ShopUserList:: update_account=%s'%(account_id))
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return

            obj_user.shop_id     = shop_id
            obj_user.disabl_edit = disabl_edit
            obj_user.regi_no   = regi_no
            obj_user.role_flag = role_flag
            obj_user.regi_type = regi_type
            obj_user.group_no  = group_no
            obj_user.put()

        if self.request.get('delete_account'):
            flag_tencyo = False
            logging.debug('ShopUserList:: delete_account=%s'%(account_id))
            if not obj_user:
                logging.error('ShopUserList:: not found user=' + account_id)
                return
            db.delete( ShopUser.all().filter("email_id =", account_id) )
            #del_account = self.request.get('del_account')
            #if del_account:
            #    obj_user = ShopUser.get_by_key_name(del_account)
            #    if obj_user:
            #        db.delete( ShopUser.all().filter("email_id =", del_account) )

        if self.request.get('append_account'):
            logging.debug('ShopUserList:: append_account=%s'%(account_id))
            if obj_user:
                logging.error('ShopUserList:: already found user=' + account_id)
                return
            if account_id and shop_id:
                obj_user = ShopUser.insert_or_fail(
                            shop_id     = shop_id,
                            email_id    = account_id,
                            disabl_edit = disabl_edit,
                            regi_no     = regi_no,
                            role_flag   = role_flag,
                            regi_type   = regi_type,
                            group_no    = group_no
                          )
        if flag_tencyo:
            obj_shop = ShopInfo.get_by_key_name(shop_id)
            if obj_shop:
                obj_shop.admin_id = account_id
                obj_shop.put()
        time.sleep(2)
        self.redirect("/sysadmin/shopuser?company_id=" + company_id)
        return


        #ASP契約

'''
店舗一覧
'''
class AllShopList(Helper):
    def get(self):
        #loginData = self.logincheck()
        delete_only_flag = self.request.get('delete_only')
        sel_todouhuken   = self.request.get('sel_todouhuken')

        shops = []
        query = None
        if delete_only_flag:
            query  = ShopInfo.all()
        if sel_todouhuken:
            if sel_todouhuken == "00":
                query  = ShopInfo.all()
            else:
                query  = ShopInfo.all().filter("shop_id >", sel_todouhuken + "000000").filter("shop_id <", sel_todouhuken + "999999")

        if query:
            for item in query:
                #if not item.urikake_card:
                #    item.urikake_card   = ["","","","","","","","","","",trans_unicode('etc', G_LANG_SEL)]
                if delete_only_flag:
                    if not item.flag_del: continue
                else:
                    if item.flag_del: continue
                shops.append(item)

        template_vals = {
                'lang':G_LANG_SEL,
                'control':translate('sysadmin', G_LANG_SEL),
                'title':"店舗全体リスト",
                'items': shops,
                'delete_only':delete_only_flag,
                'sel_todouhuken':sel_todouhuken,
                'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:NoCalendar, -9:NoConditionBar
            }
        self.helpDispatch(dir_name, 'allshoplist.html', template_vals)


    def post(self):
        loginData = self.logincheck()
        if not loginData: return

        shop_id = self.request.get('shop_id')
        delete_shop_flag = self.request.get('delete_shop_flag')

        if delete_shop_flag:
            if not shop_id:
                self.redirect("/sysadmin/allshop?delete_only=1")
                return
            obj_shop = ShopInfo.get_by_key_name(shop_id)
            if not obj_shop:
                logging.error('AllShopList:: not found shop_id =%s'%(shop_id))
                return
            if obj_shop.flag_del:
                obj_shop.flag_del = False
            else:
                obj_shop.flag_del = True
            obj_shop.put()
            time.sleep(2)
            self.redirect("/sysadmin/allshop")
            return

        if shop_id:
            email_id = loginData['shop_user'].email_id
            obj = ShopUser.get_by_key_name(email_id)
            if not obj:
                logging.error('ShopList:: not found user=' + email_id)
                return
            obj.company_key = get_mem_company(shop_id[:6])
            obj.shop_id = shop_id
            obj.put()

            userKey =  "user/" + email_id
            data = memcache.Client().delete( userKey )
            self.redirect("/report/current")
            return


'''
'''
class MakeFirstData(Helper):
    def get(self):
        logging.debug("index_controller::TestGql:: start")
        memcache.flush_all()

        obj_comp = CompanyInfo().insert_or_fail(
                            company_id  = "999999",
                            kname       = u"JoinPOS Inc.",
                            zipcode     = "162-0065",
                            address1    = u"sinjuku-ku, tokyo 162-0065, japan",
                            address2    = u"satokura-akebonobasi BLDG. 8F,sumiyoshi-cyou 1-19",
                            address3    = u"",
                            telno       = "03-6457-7172",
                            faxno       = "03-6457-7173",
                            admin_id    = "berutio"
                          )

        ShopInfo().insert_or_fail(
                            company_key = obj_comp,
                            company_id  = "999999",
                            kname       = u"JoinPOS Inc.",
                            zipcode     = "162-0065",
                            address1    = u"sinjuku-ku, tokyo 162-0065, japan",
                            address2    = u"satokura-akebonobasi BLDG. 8F,sumiyoshi-cyou 1-19",
                            address3    = u"",
                            telno       = "03-6457-7172",
                            faxno       = "03-6457-7173",
                            admin_id    = "berutio"
                          )

        ShopUser().insert_or_fail(
                            shop_id     = "99999901",
                            email_id    = "admin",
                            role_flag   = 9
                          )

        self.redirect("/")


'''
maketlog?shop_id=99990101&point_id=00101&ymd=210120
'''
class MakeTloggerHistory(Helper):
    def get(self):
        logging.debug("index_controller::TestGql:: start")
        memcache.flush_all()

        shop_id   = self.request.get('shop_id')
        point_id = self.request.get('point_id')
        ymd       = self.request.get('ymd')

        obj_tlog = TloggerHistory.get_by_key_name( "%s%s%s"%(shop_id, point_id, ymd))
        if obj_tlog:
            logging.error('%s::MakeTloggerHistory::Already Exist  shop_id=%s, point_id=%s, ymd=%s' %(ctrl_name, shop_id, point_id, ymd ))
            return

        obj_point = PointTable.get_by_key_name( "%s%s"%(shop_id, point_id))
        if not obj_point:
            logging.error('%s::MakeTloggerHistory::not found  shop_id=%s, point_id=%s' %(ctrl_name, shop_id, point_id ))
            return
        '''
        obj_history.interval = random.randint(10,20) * 30
        min = random.uniform(-5,10)
        max = random.uniform(20,80)
        obj_history.ft_alarm = [round(min, 2), round(max, 2)]  #0:alarmLow, 1:AlarmHigh
        obj_history.ft_data = [ random.uniform(min - 3, max + 3) for k in range(random.randint(500,1024))]
        obj_history.put()
        '''
        t_time = 0
        ft_data = []
        it_time = []
        ft_alarm_data = []
        it_alarm_time = []

        range_cnt = 60 / obj_point.interval
        for idx_hh in range(24):
            for idx_mm in range(60):
                for idx_ss in range(range_cnt):
                    t_data = random.uniform(obj_point.limit_dn - 1, obj_point.limit_up + 1)
                    t_time = idx_hh * 10000 + idx_mm * 100 + idx_ss * obj_point.interval
                    ft_data.append(t_data)
                    it_time.append(t_time)
                    if t_data < obj_point.limit_dn or t_data > obj_point.limit_up:
                        ft_alarm_data.append(t_data)
                        it_alarm_time.append(t_time)
        
        obj_tlog = TloggerHistory.insert_or_fail( 
                    shop_id   = shop_id,
                    ymd       = ymd,
                    point_id = point_id,
                    ft_data   = ft_data,
                    it_time   = it_time,
                    ft_alarm_data = ft_alarm_data,
                    it_alarm_time = it_alarm_time,
                    last_time = t_time
        )
        #logging.error('MakeTloggerHistory::obj_tlog before  point_id=%s' %(point_id ))
        if obj_tlog:
            #logging.error('MakeTloggerHistory::obj_tlog   point_id=%s' %(point_id ))
            obj_month = TloggerHistoryMonth.insert_or_fail( 
                        shop_id   = shop_id,
                        ym        = ymd[:4],
                        point_id = point_id,
            )
            if obj_month:
                #logging.error('MakeTloggerHistory::obj_month    point_id=%s' %(point_id ))
                day_idx = int(ymd[4:]) - 1
                data_len = len(obj_tlog.it_time)
                obj_month.it_data_cnt[day_idx]  = data_len
                obj_month.it_alarm_cnt[day_idx] = len(obj_tlog.it_alarm_time)
                #---
                max_data = -999.9
                min_data = 999.9
                sum_data = 0.0
                for idx in range(data_len):
                    sum_data += obj_tlog.ft_data[idx]
                    if obj_tlog.ft_data[idx] > max_data:
                        max_data = obj_tlog.ft_data[idx]
                    else:
                        if obj_tlog.ft_data[idx] < min_data:
                            min_data = obj_tlog.ft_data[idx]

                obj_month.it_alarm_cnt[day_idx] = len(obj_tlog.it_alarm_time)
                obj_month.ft_ave_data[day_idx]  = float("%.1f"%(sum_data / data_len))
                obj_month.ft_min_data[day_idx]  = float("%.1f"%min_data)
                obj_month.ft_max_data[day_idx]  = float("%.1f"%max_data)
                obj_month.put()
                logging.error('MakeTloggerHistory:: point_id=%s, day_idx=%d, min=%.1f, max=%.1f' %(point_id, day_idx, min_data, max_data ))

        time.sleep(1)
        '''
        if obj_tlog:
            logging.error('%s::MakeTloggerHistory::make Error  shop_id=%s, point_id=%s, ymd=%s' %(ctrl_name, shop_id, point_id, ymd ))
            return
        '''
        self.redirect("/report/list_day")


app = webapp.WSGIApplication([
    (slashdir_name,  CompanyList),
    (slashdir_name + "shoplist",      ShopList),
    (slashdir_name + "shopuser",      ShopUserList),
    (slashdir_name + "allshop",       AllShopList),
    (slashdir_name + "newcompany",    NewCompanyEdit),
    (slashdir_name + "makedata",      MakeFirstData),   #ForTEST
    (slashdir_name + "maketlog",      MakeTloggerHistory),   #ForTEST
    (slashdir_name + "tip",           UsageTip),        #ForTEST
    ], debug=True)
