# -*- coding: utf-8 -*-
from google.appengine.api import users
from google.appengine.ext import db, webapp
from google.appengine.ext.webapp import template
from google.appengine.api.labs import taskqueue
from urllib import unquote_plus
import urllib
import urllib2
import time
from google.appengine.api import urlfetch

import json
import bisect
import random # モジュールのインポート
#import math
#LOCAL
from posmodel     import *


#DiretoryInfo
dir_name = "ajax"
slash_dirname = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"

'''
'''
def func_shop_section(obj_shop):
    query_data = SectionTable.all().filter('shop_id =', obj_shop.shop_id)
    data = []
    for section in query_data:
        data.append(section.food_drink)
    obj_shop.it_section_kbn = data
    obj_shop.put()
    del_mem_shop(obj_shop.shop_id)
    return

'''
'''
def func_unicode_get(get_msg):
    return unicode(unquote_plus(str(get_msg).strip()), 'utf-8')
    #return unicode(unquote_plus(str(get_msg)), 'cp932').strip() #--->ERROR



'''
コードチェック
'''
class CheckId(webapp.RequestHandler):
    def get(self):
        tname = self.request.get('table')
        id    = self.request.get('id')

        data = None
        if   tname == "company":
            data = CompanyInfo.get_by_key_name(id)
        elif tname == "shopinfo":
            data = ShopInfo.get_by_key_name(id)
        elif tname == "shopuser":
            data = ShopUser.get_by_key_name(id)

        # 結果を返す
        if data: result = 1  #"found data "
        else:    result = 0  #"not found data "
        WriteAjaxResponse(self, lambda : json.dump(result, self.response.out))

    def post(self):
        tname    = self.request.get('table')
        gmail_id = self.request.get('gmail_id')
        shop_id  = self.request.get('shop_id')

        result = 0
        logging.info('ajax_controller::CheckId tname=%s, gmail_id=%s, shop_id=%s result=%d' % (tname, gmail_id, shop_id, result))
        results = {"result":result}
        WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))



class SectionQuery(Helper):
    #@login_required
    def post(self):
        try:
            #logging.debug("ajax_controller::SectionMaster::start")
            loginData = self.logincheck(flag=1) #flag=1は、エラー時にAJAXでエラーリターン
            if not loginData:
                logging.debug('ajax_controller::SectionMaster::LOGIN ERROR')
                user = users.get_current_user()
                if not user:
                    results = {"result":1} #LoginError
                else:
                    results = {"result":99} #・ｸ・晧譜・鰀 ・ꀀ・
                WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                return

            msg  = self.request.get('msg')
            logging.debug("ajax_controller::SectionMaster::msg=%s" %(msg))
            get_msg  = eval(msg)
            shop_id = get_msg['shop_id']
            mode    = get_msg['mode']
            sect_id = get_msg['content_id']
            result = 0
            temp_results = []
            temp_array = ["","","",1,0,0,0,0,0]
            if mode == "delete":
                obj_sect = SectionTable.get_by_key_name("%s%s"%(shop_id, sect_id))
                obj_sect.flag_del = True
                obj_sect.put()
                #タッチメニューに商品が含まれてないかを確認すること。
                temp_array[0] = sect_id
                temp_array[1] = obj_sect.kname
                temp_array[2] = obj_sect.food_drink
                temp_array[3] = obj_sect.lastno_prod
                temp_array[4] = obj_sect.image_no
                temp_array[5] = obj_sect.multi_tax
                temp_array[6] = obj_sect.common_sect
                temp_results.append(temp_array)
            else:
                kname        = func_unicode_get(get_msg['section_name'])
                food_drink   = int(get_msg.get('food_drink', "0"))
                image_no     = int(get_msg.get('image_no', "0"))
                common_sect  = int(get_msg.get('common_sect', "0"))
                #--
                flag_discount = False
                if 'flag_discount'  in get_msg:
                    flag_discount = get_msg['flag_discount']  #1:True, 0:False
                    #logging.error('ajax_controller::TouchQuery::flag_delete={}'.format( flag_delete ))
                    if flag_discount:
                        #logging.error('ajax_controller::TouchQuery::flag_delete=TRUE')
                        flag_discount = True
                    else:
                        #logging.error('ajax_controller::TouchQuery::flag_delete=FALSE')
                        flag_discount = False

                stock_flag   = get_msg.get('flag_stock', "0")
                multi_tax = 0
                if get_msg.has_key('multi_tax'):
                    multi_tax   = int(get_msg['multi_tax'])
                #logging.debug("ajax_controller::SectionMaster::shop_id=%s, mode=%s, sect_id=%s, kname=%s" %(shop_id, mode, sect_id, kname))
                obj_shop = get_mem_shop(shop_id)
                if mode == "insert": #insert
                    obj_sect = SectionTable.insert_or_fail(
                                    shop_id    = shop_id,
                                    kname      = kname,
                                    food_drink = food_drink,
                                    image_no   = image_no,
                                    multi_tax  = multi_tax,
                                    flag_discount = flag_discount,
                                    common_sect   = common_sect
                                )
                    temp_array[0] = obj_sect.sect_id
                    temp_array[1] = obj_sect.kname
                    temp_array[2] = obj_sect.food_drink
                    temp_array[3] = obj_sect.lastno_prod
                    temp_array[4] = obj_sect.image_no
                    temp_array[5] = obj_sect.multi_tax
                    temp_array[6] = obj_sect.common_sect
                    if obj_sect.flag_stock:
                        temp_array[7] = 1
                    if obj_sect.flag_discount:
                        temp_array[8] = 1
                    temp_results.append(temp_array)
                elif  mode == "update": #update
                    obj_sect = get_mem_section(shop_id + sect_id)
                    if stock_flag == "1":
                        obj_sect.flag_stock = True
                    else:
                        obj_sect.kname       = kname
                        obj_sect.food_drink  = food_drink
                        obj_sect.image_no    = image_no
                        obj_sect.multi_tax   = multi_tax
                        obj_sect.common_sect = common_sect
                        obj_sect.flag_del      = False
                        obj_sect.flag_stock    = False
                        obj_sect.flag_discount = flag_discount
                    obj_sect.put()
                    temp_array[0] = sect_id
                    temp_array[1] = kname
                    temp_array[2] = obj_sect.food_drink
                    temp_array[3] = obj_sect.lastno_prod
                    temp_array[4] = obj_sect.image_no
                    temp_array[5] = obj_sect.multi_tax
                    temp_array[6] = obj_sect.common_sect
                    if obj_sect.flag_stock:
                        temp_array[7] = 1
                    if obj_sect.flag_discount:
                        temp_array[8] = 1
                    temp_results.append(temp_array)
                #elif  mode == "delete": #delete
                elif  mode == "query": #Query All
                    for item in obj_shop.sectiontable_set:
                        if item.flag_del: continue
                        temp_array = ["","","",1,0,0,0,0,0]
                        temp_array[0] = item.sect_id
                        temp_array[1] = item.kname
                        temp_array[2] = item.food_drink
                        temp_array[3] = item.lastno_prod
                        temp_array[4] = item.image_no
                        temp_array[5] = obj_sect.multi_tax
                        temp_array[6] = obj_sect.common_sect
                        if obj_sect.flag_stock:
                            temp_array[7] = 1
                        if obj_sect.flag_discount:
                            temp_array[8] = 1
                        temp_results.append(temp_array)
                else:
                    result = 2
                if obj_sect and obj_sect.image_no:
                    for record in obj_sect.producttable_set:
                        if record.image_no and record.image_no == obj_sect.image_no:
                            continue
                        record.image_no = obj_sect.image_no
                        record.put()
                # 結果を返す
                if mode == "insert" or  mode == "update": #insert
                    func_shop_section(obj_shop)
                    #MasterUpdateDate Setting
                    #
            results = {"result":result, "data":temp_results} #OK
            #------------
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
        except Exception as e:
            logging.error('ajax_controller::SectionQuery::ERROR=' + e.message)
            results = {"result":3}
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
            raise

'''
'''
class ShopQuery(Helper):
    #@login_required
    def post(self):
        try:
            #logging.debug("ajax_controller::ShopQuery::start")
            loginData = self.logincheck(flag=1) #flag=1は、エラー時にAJAXでエラーリターン
            if not loginData:
                logging.debug('ajax_controller::ShopQuery::LOGIN ERROR')
                user = users.get_current_user()
                if not user:
                    results = {"result":1} #LoginError
                else:
                    results = {"result":99} #・ｸ・晧譜・鰀 ・ꀀ・
                WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                return
            company_id = loginData['shop_user'].shop_id[0:6]

            msg  = self.request.get('msg')
            #logging.debug("ajax_controller::ShopQuery::msg=%s" %(msg))
            get_msg  = eval(msg)
            mode     = get_msg['mode']
            shop_id  = get_msg['content_id']
            #logging.debug("ajax_controller::ShopQuery::msg=%s" %(msg))

            temp_results = []
            result = 0
            if mode == "insert": #insert
                obj = ShopInfo.insert_or_fail(
                                company_id = company_id,
                                kname    = func_unicode_get(get_msg['shop_name']),
                                admin_id = func_unicode_get(get_msg['admin_id'])
                            )
                temp_array = ["","",""]
                temp_array[0] = obj.shop_id
                temp_array[1] = obj.kname
                temp_array[2] = obj.admin_id
                temp_results.append(temp_array)
            elif  mode == "delete": #delete
                ret = ShopInfo.delete_with_ref(shop_id=shop_id)
                if not ret:
                    logging.debug("ajax_controller::ShopQuery::delete FALSE")
                    result = 2
            else:
                result = 2
            results = {"result":result, "data":temp_results} #OK
            # 結果を返す
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
        except Exception as e:
            logging.error('ajax_controller::ShopQuery::ERROR=' + e.message)
            results = {"result":3}
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
            raise

'''
'''
class ProductQuery(Helper):
    #@login_required
    def post(self):
        try:
            #logging.errorg('ajax_controller::ProductQuery....test1')
            loginData = self.logincheck(flag=1) #flag=1は、エラー時にAJAXでエラーリターン
            if not loginData:
                #logging.debug('ajax_controller::ProductQuery::LOGIN ERROR')
                user = users.get_current_user()
                if not user:
                    results = {"result":1} #LoginError
                else:
                    results = {"result":99} #・ｸ・晧譜・鰀 ・ꀀ・
                WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                return
            msg = self.request.get('msg')
            #logging.debug("ajax_controller::ProductQuery::msg=%s" %(msg))
            get_msg = eval(msg)
            shop_id = get_msg['shop_id']
            mode    = get_msg['mode']
            plucode = get_msg['content_id']

            main_obj_shop = get_mem_shop(shop_id)
            temp_results = []
            result = 0
            if mode == "insert" or mode == "update" or mode == "delete": #insert
                sect_id = get_msg['sect_id']
                if mode == "insert" or mode == "update":
                    kname      = func_unicode_get(get_msg['kname'])
                    kname2     = func_unicode_get(get_msg['kname2'])
                    price_unit = int_currency(get_msg['price_unit']) #小売単価
                    #
                    #2012/11/03 TAXは店舗共通で使い、商品別にはＴａｘコードを持たない。
                    #tax_id     = get_msg['tax_id']    #税id
                    tax_id = '1' #既存ソースを修正しないため、Defaultで１にする。

                    st_option_id   = [get_msg['option1'],
                                      get_msg['option2'],
                                      get_msg['option3']]  #OptionID(最大３つ）
                    printer_id = int(get_msg['printer_id']) #printer区分
                    changeable = get_msg['changeable'] #単価調整(0:不可, 1:可能）
                    if changeable: changeable = int(changeable)
                    else: changeable = 0
                    #
                    etc_char = int(get_msg['etc_char']) #
                    #logging.error("ProductQuery:: kname=%s, etc_char=%d"%( kname, etc_char))

                    temp  = get_msg['price_raw']  #原価
                    if temp: price_raw = int_currency(temp)
                    else:    price_raw = 0

                    temp = get_msg['priority']
                    if temp: priority = int(temp)
                    else:    priority = 50

                    temp = get_msg['price_reserved']
                    if temp: price_reserved = int_currency(temp)
                    else:    price_reserved = 0
                    #
                    course_cd = get_msg.get('course_cd', '')

                    option_tabid = 0
                    if 'tab_option' in get_msg:
                        temp = get_msg['tab_option']
                        if temp: option_tabid = int(temp)

                obj_sect = get_mem_section(shop_id + sect_id)
                if not obj_sect:
                    results = {"result":2, "data":temp_results} #OK
                    WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                    return

                shops = []
                if obj_sect.common_sect:
                    obj_comp = main_obj_shop.company_key
                    if obj_comp:
                        #shops = obj_comp.shopinfotable_set
                        for item in obj_comp.shopinfo_set:
                            if item.flag_del: continue
                            shops.append(item.shop_id)
                else:
                    shops.append(shop_id)


                if mode == "insert": #insert
                    for tmp_shop_id in shops:
                        obj_shop = get_mem_shop(tmp_shop_id)
                        obj_prod = PointTable.insert_or_fail(
                                        shop_id    = tmp_shop_id, #shop_id,
                                        sect_id    = sect_id,
                                        #plucode = plucode,
                                        kname      = kname,
                                        kname2     = kname2,
                                        price_unit = price_unit,
                                        price_raw  = price_raw,
                                        tax_id     = tax_id,
                                        st_option_id = st_option_id,
                                        printer_id   = printer_id,
                                        changeable   = changeable,
                                        priority     = priority,
                                        price_reserved = price_reserved,
                                        course_cd    = course_cd,
                                        etc_char     = etc_char,
                                        option_tabid = option_tabid
                                    )
                if mode == "update": #
                    for tmp_shop_id in shops:
                        obj_shop = get_mem_shop(tmp_shop_id)
                        obj_prod = PointTable.get_by_key_name(tmp_shop_id + plucode)
                        if not obj_prod:
                            logging.error("ajax_controller::ProductQuery::not found shop=%s, plucode=%s" %(tmp_shop_id,plucode))
                            results = {"result":3}
                            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                            return
                        if obj_prod.kname != kname or obj_prod.kname2 != kname2:
                            obj_prod.kname    = kname
                            obj_prod.kname2   = kname2
                            #self.TouchMenuTableUpdate("update", obj_prod, obj_shop) #2018/07/29 タッチメニューの名称はレジにて商品名を参照
                        obj_prod.price_unit   = price_unit
                        obj_prod.price_raw    = price_raw
                        obj_prod.tax_id       = tax_id
                        obj_prod.st_option_id = st_option_id
                        obj_prod.printer_id   = printer_id
                        obj_prod.changeable   = changeable
                        obj_prod.priority     = priority
                        obj_prod.price_reserved = price_reserved
                        obj_prod.course_cd    = course_cd
                        obj_prod.flag_del     = False
                        obj_prod.etc_char     = etc_char
                        obj_prod.option_tabid = option_tabid
                        obj_prod.put()

                if mode == "delete": #insert
                    for tmp_shop_id in shops:
                        obj_shop = get_mem_shop(tmp_shop_id)
                        obj_prod = PointTable.get_by_key_name(tmp_shop_id + plucode)
                        if not obj_prod:
                            logging.error("ajax_controller::ProductQuery::not found shop=%s, plucode=%s" %(tmp_shop_id,plucode))
                            results = {"result":3}
                            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
                            return
                        obj_prod.kname2   = u"削除"
                        obj_prod.image    = None
                        obj_prod.flag_del = True
                        obj_prod.put()

                    self.TouchMenuTableUpdate("delete", obj_prod, obj_shop)

                temp_array = ["","","","","","","","","","","","",0,0]
                temp_array[0] = obj_prod.plucode
                temp_array[1] = obj_prod.kname
                temp_array[2] = obj_prod.price_unit
                temp_array[3] = obj_prod.price_raw
                temp_array[4] = obj_prod.tax_id
                temp_array[5] = obj_prod.st_option_id
                temp_array[6] = obj_prod.printer_id
                temp_array[7] = obj_prod.changeable
                if obj_prod.kname2:
                    temp_array[8] = obj_prod.kname2
                temp_array[9]  = obj_prod.priority
                temp_array[10] = obj_prod.price_reserved #予約小売単価   #20130427
                if obj_prod.course_cd > "": temp_array[11] = obj_prod.course_cd
                if obj_prod.etc_char:
                    temp_array[12] = obj_prod.etc_char
                if obj_prod.option_tabid:
                    temp_array[13] = obj_prod.option_tabid
                temp_results.append(temp_array)
            elif  mode == "query": #Query All
                sect_id = get_msg['content_id']
                obj_sect = get_mem_section(shop_id + sect_id)
                for item in obj_sect.producttable_set:
                    if item.flag_del: continue
                    temp_array = ["","","", 0, 0]
                    temp_array[0] = item.plucode
                    temp_array[1] = item.kname
                    if item.kname2:
                        temp_array[2] = item.kname2
                    temp_results.append(temp_array)
                    if item.current_stock:
                        temp_array[3] = item.current_stock
                    temp_array[4] = item.price_unit
            else:
                result = 2
            #
            results = {"result":result, "data":temp_results} #OK
            # 結果を返す
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
        except Exception as e:
            logging.error('ajax_controller::ProductQuery::ERROR=' + e.message)
            results = {"result":3}
            WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))
            raise


'''
ShopUser Check
'''
class ShopUserCheck(Helper):
    def get(self):
        company_id = self.request.get('company_id')
        user_id    = self.request.get('user_id')
        kbn        = self.request.get('kbn')  #insert,  update
        err_flag = 0
        entity = ShopUser().get_by_key_name(user_id)
        if entity:
            if kbn=='insert': err_flag = 1
            else:
                if company_id != entity.company_id: err_flag = 1
        else:
            if kbn=='update':
                logging.error('ajax_controller::ShopUserCheck::not found shopuser' + user_id)

        results = {"result":err_flag}  #login fail
        WriteAjaxResponse(self, lambda : json.dump(results, self.response.out))



def WriteAjaxResponse(handler, writer):
    handler.response.headers['Access-Control-Allow-Origin'] = '*'
    handler.response.headers['Access-Control-Allow-Headers'] = '*'
    handler.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    handler.response.headers['Content-Type'] = 'application/json'
    writer()


app = webapp.WSGIApplication([
    ("/ajax/check",       CheckId),
    #
    ("/ajax/section",     SectionQuery),
    ("/ajax/product",     ProductQuery),
    ("/ajax/shop",        ShopQuery),
    ("/ajax/shopuser",    ShopUserCheck),
    #
    ], debug=True)
