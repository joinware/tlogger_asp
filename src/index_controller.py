# -*- coding: utf-8 -*-
import os
#import logging
#import StringIO

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users

#For Transrate
from posmodel import *
webapp.template.register_template_library('filters.filter')


class SelectApp(Helper):
    def get(self):
        #loginData = self.logincheck()
        loginData = ""
        obj_comp = ""
        lang_sel = G_LANG_SEL
        user = users.get_current_user()
        if user:
            loginData = {'url':users.create_logout_url("/"),
                         'user':user,
                         'shop_user':None,
                         'shop_info':None,
                         'url_linktext':'Logout'}
            objShopUser = get_mem_user(user)
            if objShopUser:
                if objShopUser.lang_sel:
                    lang_sel = objShopUser.lang_sel
                else:
                    objShopUser.lang_sel = G_LANG_SEL
                    objShopUser.put()
                    del_mem_user(user)

                loginData['shop_user'] = objShopUser
                loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)
                obj_comp = get_mem_company(objShopUser.shop_id[:6])
        ####
        asp_flag = [0,0,0,0,0,0,0,0,0]
        title_string = u"温度管理"

        loginUrl = users.create_login_url("/")
        template_vals = {
                'lang':lang_sel,
                'title':title_string,
                'loginData':loginData,
                'loginUrl':loginUrl,
                'obj_comp':obj_comp,
                'asp_flag':asp_flag
            }
        self.helpDispatch('init', 'index.html', template_vals)

    def post(self):
        user = users.get_current_user()

        lang_sel = self.request.get('lang_sel')
        objShopUser = get_mem_user(user)
        objShopUser.lang_sel = lang_sel
        objShopUser.put()
        del_mem_user(user)

        if user:
            loginData = {'url':users.create_logout_url("/"),
                         'user':user,
                         'shop_user':None,
                         'shop_info':None,
                         'url_linktext':'Logout'}
            if objShopUser:
                loginData['shop_user'] = objShopUser
                loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)
        ####
        asp_flag = [0,0,0,0,0,0,0,0,0]
        title_string = u"JoinPOS"

        loginUrl = users.create_login_url("/")
        template_vals = {
                'lang':loginData['shop_user'].lang_sel,
                'title':title_string,
                'loginData':loginData,
                'loginUrl':loginUrl,
                'asp_flag':asp_flag
            }
        self.helpDispatch('init', 'index.html', template_vals)


'''
'''
class GoogleVerify(Helper):
    def get(self):
        template_vals = ""

        self.helpDispatch("init", 'googlef8108259e5ef1aa6.html', template_vals)

'''
'''
class GoogleSiteMap(Helper):
    def get(self):
        server_url  = get_mem_servername()
        baseurl     = "http://" + server_url + "/"
        introUrl    = baseurl + "intro/"
        reportUrl   = baseurl + "report/"
        masterUrl   = baseurl + "master/"
        reserveUrl  = baseurl + "reserve/"
        setupUrl    = baseurl + "setup/"
        timecardUrl = baseurl + "timecard/"
        url_array = [
                     baseurl,
                     introUrl,
                     introUrl + "about",
                     introUrl + "sikumi",
                     introUrl + "option",
                     introUrl + "example",
                     introUrl + "qna",
                     introUrl + "faq",
                     introUrl + "history",
                     introUrl + "option",
                     introUrl + "reason",
                     introUrl + "character",
                     #
                     reportUrl,
                     reportUrl   + "current",
                     reportUrl   + "week",
                     reportUrl   + "pay",
                     reportUrl   + "custom",
                     reportUrl   + "product",
                     reportUrl   + "month",
                     reportUrl   + "year",
                     reportUrl   + "fl",
                     reportUrl   + "inputcost",
                     reportUrl   + "journal",
                     reportUrl   + "receipt",
                     reportUrl   + "daily",
                     reportUrl   + "auditdaily",
                     reportUrl   + "auditmonth",
                     reportUrl   + "service",
                     reportUrl   + "spdaily",
                     reportUrl   + "manual",
                     #
                     masterUrl,
                     masterUrl   + "touchlist",
                     masterUrl   + "touchdetail",
                     masterUrl   + "section",
                     masterUrl   + "product",
                     masterUrl   + "productall",
                     masterUrl   + "spedit",
                     masterUrl   + "option",
                     masterUrl   + "course",
                     masterUrl   + "staff",
                     #
                     reserveUrl,
                     reserveUrl  + "edit",
                     reserveUrl  + "calendar",
                     reserveUrl  + "setup",
                     reserveUrl  + "timelist",
                     reserveUrl  + "daylist",
                     reserveUrl  + "custedit",
                     reserveUrl  + "custsel",
                     reserveUrl  + "msgedit",
                     #
                     setupUrl,
                     setupUrl    + "shoplist",
                     setupUrl    + "shopedit",
                     setupUrl    + "shopseat",
                     setupUrl    + "shopoption",
                     setupUrl    + "csvread",
                     setupUrl    + "productcsv",
                     setupUrl    + "aspreceipt",
                     #
                     timecardUrl,
                     timecardUrl + "dakoku",
                     timecardUrl + "daily",
                     timecardUrl + "dailyedit",
                     timecardUrl + "month",
                     timecardUrl + "shopmonth",
                     timecardUrl + "stafflist",
                     timecardUrl + "staffedit",
                     timecardUrl + "setup",
                     ]
        now = getJST()
        ymd = "%04d%02d%02d" % (now.year, now.month, 1)

        udate = "2014-09-30"
        template_vals = {
                'url_array':url_array,
                'udate':ymd,
            }
        self.response.headers['Content-Type'] = 'application/xml'
        self.helpDispatch("init", 'sitemap.xml', template_vals)

app = webapp.WSGIApplication([
    ('/',     SelectApp),
    ("/googlef8108259e5ef1aa6.html", GoogleVerify),
    ("/sitemap.xml",    GoogleSiteMap),
    ], debug=True)

