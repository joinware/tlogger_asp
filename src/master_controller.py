# -*- coding: utf-8 -*-
#import operator
from google.appengine.api import users
from google.appengine.ext import db, webapp
from google.appengine.ext.webapp import template
from google.appengine.api import images
#from operator import attrgetter
from posmodel import *
webapp.template.register_template_library('filters.filter')
import time
#LOCAL

#DiretoryInfo
dir_name  = "master"
slash_dir = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"


'''
'''
def func_sjis_decode( data ):
    return data
    '''
    try:
        return data.encode('CP932').decode('SJIS')
        #return data.encode('CP932')
    except Exception as e:
        return "??"
    '''
'''
'''
def func_multi_country( st_multi_lang, lang_sel ):
    for idx in range(len(st_multi_lang)):
        if st_multi_lang[idx] == "jp": st_multi_lang[idx]  = translate('jp', lang_sel)
        if st_multi_lang[idx] == "ko": st_multi_lang[idx]  = translate('ko', lang_sel)
        if st_multi_lang[idx] == "en": st_multi_lang[idx]  = "ENGLISH"                 #u"英語"
        if st_multi_lang[idx] == "zh": st_multi_lang[idx]  = translate('zh', lang_sel)
    return st_multi_lang


'''
'''
class SectionEdit(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            shop_cnt = len(shops)
            sections = obj_shop.sectiontable_set

            if self.request.get('csv'):
                now = getJST()
                self.response.headers['Content-Type'] = "application/x-csv; charset=UTF-8"
                self.response.headers['Content-Disposition'] = "attachment; filename=section_"+now.strftime("%Y%m%d")+".csv"
                change_utf8n(self)
                title = u"部門コード,商門名,区分"
                self.response.out.write(title  + "\r\n")

                for record in sections:
                    item = ["","",""]
                    item[0] = record.sect_id
                    item[1] = func_sjis_decode(record.kname)
                    if record.food_drink == 1:
                        item[2] = u"ドリンク"
                    else:
                        item[2] = u"フード"
                    self.response.out.write(','.join(item) + "\r\n")
                return


            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('master', lang_sel),
                    'title':translate('section_master', lang_sel), #"部門マスター",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shop': obj_shop,
                    'shops': shops,
                    'shop_cnt':shop_cnt,
                    'sections': sections,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                    'csvdownload':2, #CSVダウンロードボタン追加
                }
            self.helpDispatch("primary", 'master_section.html', template_vals)
        except UserException as e:
            occur_point = "%s::SectionEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::SectionEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


'''
'''
class StaffEdit(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

            staffs = obj_shop.staffuser_set

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('master', lang_sel),
                    'title':translate('staff', lang_sel),
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'staffs': staffs,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'master_staff.html', template_vals)
        except UserException as e:
            occur_point = "%s::StaffEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::StaffEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise




'''
'''
class PointAllList(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

            #logging.debug('%s::ProductAllEdit::shop_id=%s' %(ctrl_name, obj_shop.shop_id ))
            #sect_id  = self.request.get('sect_id')
            obj_sect = ""

            products = []
            query_data = PointTable.all().filter('shop_id =', shop_id)
            for item in query_data:
                products.append(item)

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('master', lang_sel),
                    'title':translate('product_all', lang_sel),
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'sect': obj_sect,
                    'products': products,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'master_pointlist.html', template_vals)
        except UserException as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise
'''
'''
class PointAllEdit(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)

            #logging.debug('%s::ProductAllEdit::shop_id=%s' %(ctrl_name, obj_shop.shop_id ))
            #sect_id  = self.request.get('sect_id')
            obj_sect = ""

            idx_cnt = 0
            products = []
            query_data = PointTable.all().filter('shop_id =', shop_id)
            #query_data = DeviceTable.all().filter('shop_id =', shop_id)
            for item in query_data:
                products.append(item)
                idx_cnt += 1
            for idx in range(idx_cnt,20):
                item = PointTable()
                products.append(item)

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('master', lang_sel),
                    'title':translate('product_all', lang_sel),
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'sect': obj_sect,
                    'products': products,
                    'sel_period':0,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'master_product_all_tsv.html', template_vals)
        except UserException as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise

    def post(self):
        try:
            shop_id = self.request.get('shop_id')
            #sect_id = self.request.get('section_select')
            sect_id = "001"
            list_point_id = self.request.get_all('point_id')
            list_kname    = self.request.get_all('kname')
            list_limit_up = self.request.get_all('limit_up')
            list_limit_dn = self.request.get_all('limit_dn')
            list_interval = self.request.get_all('interval')
            list_location = self.request.get_all('location')
            list_comment  = self.request.get_all('comment')
            #--
            #print(list_limit_up)
            #logging.error('%s::PointAllEdit::shop_id=%s' %(ctrl_name, shop_id ))
            #obj_sect = SectionTable.get_by_key_name(shop_id + sect_id)
            for idx, point_id in enumerate(list_point_id):
                point_id = list_point_id[idx].strip()
                if not point_id: continue
                obj_device = PointTable.get_by_key_name("%s%s"%(shop_id, point_id))
                if obj_device:
                    obj_device.point_id = point_id
                    obj_device.kname    = list_kname[idx].strip()
                    obj_device.limit_up = float(list_limit_up[idx].strip())
                    obj_device.limit_dn = float(list_limit_dn[idx].strip())
                    obj_device.interval = int(list_interval[idx].strip())
                    obj_device.location = list_location[idx].strip()
                    obj_device.comment  = list_comment[idx].strip()
                    obj_device.put()
                else:
                    obj_device = PointTable.insert_or_fail(
                        shop_id = shop_id,
                        sect_id = sect_id,
                        point_id = point_id,
                        kname     = list_kname[idx].strip(),
                        limit_up  = float(list_limit_up[idx].strip()),
                        limit_dn  = float(list_limit_dn[idx].strip()),
                        interval  = int(list_interval[idx].strip()),
                        location  = list_location[idx].strip(),
                        comment   = list_comment[idx].strip()
                    )
            #
            time.sleep(1)
            self.redirect(slash_dir + "deviceall" )
        except UserException as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::ProductAllEdit::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


app = webapp.WSGIApplication([
    (slash_dir,   PointAllList),
    (slash_dir + "section",     SectionEdit),
    (slash_dir + "list",        PointAllList),
    (slash_dir + "deviceall",   PointAllEdit),
    (slash_dir + "staff",       StaffEdit),
    ], debug=True)
