from google.appengine.ext import webapp
from posmodel import translate, currency, currency_form

register = webapp.template.create_template_register()

@register.filter("tr")
def translateLang(obj, langs='jp', reload=False):
    return translate(obj, langs, reload)

@register.filter("currency")
def currencyNumber(obj):
    return currency(obj)

@register.filter("money_form")
def currencyFormNumber(obj):
    return currency_form(obj, True)

@register.filter("money_form_blank")
def currencyFormNumberBlank(obj):
    return currency_form(obj, False)

@register.filter
def quotientString(value, arg):
    if arg == 0: return "00"
    return "%02d"%(int(value) / int(arg))

@register.filter
def remainderString(value, arg):
    if arg == 0: return "00"
    return "%02d"%(int(value) % int(arg))

@register.filter
def additionNumber(value, arg):
    return "%d"%(int(value) + int(arg))

@register.filter
def subtractionNumber(value, arg):
    return "%d"%(int(value) - int(arg))

@register.filter
def multiplicationNumber(value, arg):
    return "%d"%(int(value) * int(arg))

@register.filter
def divisionNumber(value, arg):
    if arg == 0: return "0"
    return "%d"%(int(int(value) / int(arg)))
