# -*- coding: utf-8 -*-
from google.appengine.api import users
from google.appengine.ext import db, webapp
from google.appengine.ext.webapp import template
import calendar
import random # モジュールのインポート
import time

#LOCAL
from posmodel import *
webapp.template.register_template_library('filters.filter')

import json
from time import sleep

#DiretoryInfo
dir_name  = "report"
slash_dir = '/' + dir_name + '/'
ctrl_name = dir_name + "_controller"

MAX_DAY_STAFF = 20

'''
'''
def func_week_translatee( lang_sel ):
    week_name = ["" for j in range(7)]
    week_name[0] = trans_unicode('mon', lang_sel)
    week_name[1] = trans_unicode('tue', lang_sel)
    week_name[2] = trans_unicode('wed', lang_sel)
    week_name[3] = trans_unicode('thu', lang_sel)
    week_name[4] = trans_unicode('fri', lang_sel)
    week_name[5] = trans_unicode('sat', lang_sel)
    week_name[6] = trans_unicode('sun', lang_sel)
    return week_name

'''
'''
class HistoryListDay(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            start_date = self.request.get('startDate').replace("/","")
            if not start_date:
                start_date = getJST().strftime("%Y%m%d")

            items = []
            query_data = PointTable.all().filter('shop_id =', shop_id)
            for item in query_data:
                tmp = [ item.point_id, item.kname, "%.1f ~ %.1f"%(item.limit_dn, item.limit_up), 0, "", "", 0, "", "", 0, 0, 0]
                obj_hist = TloggerHistory.get_by_key_name("%s%s%s"%(shop_id, item.point_id, start_date[2:]))
                if obj_hist:
                    alarm_len = len(obj_hist.it_alarm_time)
                    max_data = -999.9
                    min_data = 999.9
                    for idx in range(alarm_len):
                        if obj_hist.ft_alarm_data[idx] > max_data:
                            max_data = obj_hist.ft_alarm_data[idx]
                        else:
                            if obj_hist.ft_alarm_data[idx] < min_data:
                                min_data = obj_hist.ft_alarm_data[idx]
                    #---
                    data_len = len(obj_hist.it_time)
                    sum_data = 0.0
                    if max_data == -999.9 or min_data == 999.9:
                        for idx in range(data_len):
                            sum_data += obj_hist.ft_data[idx]
                            if obj_hist.ft_data[idx] > max_data:
                                max_data = obj_hist.ft_data[idx]
                            else:
                                if obj_hist.ft_data[idx] < min_data:
                                    min_data = obj_hist.ft_data[idx]
                    else:
                        for idx in range(data_len):
                            sum_data += obj_hist.ft_data[idx]

                    ave_data = sum_data / data_len
                    if data_len > 0:
                        tmp[3] = data_len
                        tmp[4] = "%06d" % obj_hist.it_time[0]
                        tmp[5] = "%06d" % obj_hist.it_time[data_len - 1]
                    #
                    if alarm_len > 0:
                        tmp[6] = alarm_len
                        tmp[7] = "%06d" % obj_hist.it_alarm_time[0]
                        tmp[8] = "%06d" % obj_hist.it_alarm_time[alarm_len - 1]
                    tmp[9]  = float("%.1f"%ave_data)
                    tmp[10] = float("%.1f"%max_data)
                    tmp[11] = float("%.1f"%min_data)
                items.append(tmp)
                logging.error('%s::HistoryList::shop_id=%s, point_id=%s, ym=%s' %(ctrl_name, shop_id, item.point_id, start_date ))

            #query_data = TloggerHistory.all().filter('shop_id =', shop_id).filter('point_id =', point_id).filter('ymd >', start_date + "00").filter('ymd <', start_date + "99")
            #query_data = TloggerHistory.all().filter('shop_id =', shop_id).filter('ymd =', start_date[2:])

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('report', lang_sel),
                    'title':"日別一覧",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'start_date': start_date,
                    'items': items,
                    'sel_period':3,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'report_history_day.html', template_vals)
        except UserException as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


'''
'''
class HistoryPointDay(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            start_date = self.request.get('startDate').replace("/","")
            if not start_date:
                start_date = getJST().strftime("%Y%m%d")
            point_id = self.request.get('point_id')
            lang_sel = loginData['shop_user'].lang_sel

            if self.request.get('csv'):
                csvdatakey =  "pointday/" + shop_id + point_id
                display_table = memcache.get(csvdatakey)
                if not display_table:
                    raise UserException( translate('error_csv', lang_sel) )
                now = getJST()
                self.response.headers['Content-Type'] = "application/x-csv; charset=UTF-8"
                self.response.headers['Content-Disposition'] = "attachment; filename=point_day_"+now.strftime("%Y%m%d")+".csv"
                change_utf8n(self)

                #月間売上げ
                title = u"時刻,温度"
                self.response.out.write(title  + "\r\n")
                for item in display_table:
                    self.response.out.write("%s:%s:%s,%s\r\n" % (item[0][:2], item[0][2:4], item[0][4:], item[1]))
                self.response.out.write("\r\n\r\n")


            obj_point = PointTable.get_by_key_name("%s%s"%(shop_id, point_id))
            if not obj_point:
                logging.error('%s::HistoryList:: not found PointTable shop_id=%s, point_id=%s' %(ctrl_name, shop_id, point_id ))
                return

            obj_hist = TloggerHistory.get_by_key_name("%s%s%s"%(shop_id, point_id, start_date[2:]))
            if not obj_hist:
                logging.error('%s::HistoryList:: not found TloggerHistory shop_id=%s, point_id=%s, ymd=%s' %(ctrl_name, shop_id, point_id, start_date ))
                return
            items = []
            for idx in range(len(obj_hist.it_time)):
                items.append( [ "%06d" % obj_hist.it_time[idx], float("%.1f" %obj_hist.ft_data[idx])  ])

            csvdatakey =  "pointday/" + shop_id + point_id
            memcache.set(key = csvdatakey, value = items, time = 600)
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('report', lang_sel),
                    'title':"POINT詳細",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'start_date': start_date,
                    'obj_point':obj_point,
                    'point_id': point_id,
                    'limit_up': float("%.1f"%obj_point.limit_up),
                    'limit_dn': float("%.1f"%obj_point.limit_dn),
                    'items': items,
                    'csvdownload':1, #CSVダウンロードボタン追加
                    'sel_period':3,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'report_history_day_chart.html', template_vals)
        except UserException as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


'''HistoryListMonth
'''
class HistoryListMonth(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            start_date = self.request.get('startDate').replace("/","")
            if not start_date:
                start_date = getJST().strftime("%Y%m")

            items = []
            query_data = PointTable.all().filter('shop_id =', shop_id)
            for item in query_data:
                tmp = [ item.point_id, item.kname, "%.1f ~ %.1f"%(item.limit_dn, item.limit_up), 0, 0, 0, 0, 0, 0]
                obj_hist = TloggerHistoryMonth.get_by_key_name("%s%s%s"%(shop_id, item.point_id, start_date[2:6]))
                if obj_hist:
                    sum_data_cnt  = 0
                    sum_alarm_cnt = 0
                    ave_data = 0.0
                    max_data = 0.0
                    min_data = 0.0
                    cnt = 0
                    for idx in range(31):
                        if obj_hist.it_data_cnt[idx] == 0: continue
                        sum_data_cnt  += obj_hist.it_data_cnt[idx]
                        sum_alarm_cnt += obj_hist.it_alarm_cnt[idx]
                        cnt += 1
                        ave_data += obj_hist.ft_ave_data[idx]
                        min_data += obj_hist.ft_min_data[idx]
                        max_data += obj_hist.ft_max_data[idx]
                    tmp[3] = ave_data / cnt
                    tmp[4] = min_data / cnt
                    tmp[5] = max_data / cnt
                    tmp[6] = sum_data_cnt
                    tmp[7] = sum_alarm_cnt
                items.append(tmp)
                logging.error('%s::HistoryList::shop_id=%s, point_id=%s, ym=%s' %(ctrl_name, shop_id, item.point_id, start_date ))

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('report', lang_sel),
                    'title':"月間一覧",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'start_date': start_date,
                    'items': items,
                    'sel_period':4,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'report_history_month.html', template_vals)
        except UserException as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


'''HistoryPointMonth
'''
class HistoryPointMonth(Helper):
    def get(self):
        try:
            loginData = self.logincheck()
            if not loginData: return
            shop_id, obj_shop, shops = func_get_common_shopinfo(self, loginData)
            lang_sel = loginData['shop_user'].lang_sel

            start_date = self.request.get('startDate').replace("/","")
            if not start_date:
                start_date = getJST().strftime("%Y%m")
            point_id = self.request.get('point_id')

            obj_point = PointTable.get_by_key_name("%s%s"%(shop_id, point_id))
            if not obj_point:
                logging.error('%s::HistoryPointMonth:: not found PointTable shop_id=%s, point_id=%s' %(ctrl_name, shop_id, point_id ))
                return
            #
            week_name = func_week_translatee( lang_sel )
            tmp = calendar.monthrange(int(start_date[:4]), int(start_date[4:6]))
            week_first = tmp[0]
            month_end  = tmp[1]
            #logging.error('%s::HistoryPointMonth:: week_name=%s,  %d, %d' %(ctrl_name, week_name, tmp[0], tmp[1] ))
            #
            items = []
            for idx in range(1,month_end+1):
                tmp = [ "%02d"%idx, week_name[week_first], "", 0, "", "", 0, "", "", 0, 0, 0]
                week_first += 1
                if week_first > 6: week_first = 0

                logging.error("TloggerHistory :: %s%s%s%02d"%(shop_id, point_id, start_date[2:6], idx))

                obj_hist = TloggerHistory.get_by_key_name("%s%s%s%02d"%(shop_id, point_id, start_date[2:6], idx))
                if obj_hist:
                    logging.error("Found TloggerHistory :: %s%s%s%02d"%(shop_id, point_id, start_date[2:6], idx))
                    alarm_len = len(obj_hist.it_alarm_time)
                    max_data = -999.9
                    min_data = 999.9
                    for idx in range(alarm_len):
                        if obj_hist.ft_alarm_data[idx] > max_data:
                            max_data = obj_hist.ft_alarm_data[idx]
                        else:
                            if obj_hist.ft_alarm_data[idx] < min_data:
                                min_data = obj_hist.ft_alarm_data[idx]
                    #---
                    data_len = len(obj_hist.it_time)
                    sum_data = 0.0
                    if max_data == -999.9 or min_data == 999.9:
                        for idx in range(data_len):
                            sum_data += obj_hist.ft_data[idx]
                            if obj_hist.ft_data[idx] > max_data:
                                max_data = obj_hist.ft_data[idx]
                            else:
                                if obj_hist.ft_data[idx] < min_data:
                                    min_data = obj_hist.ft_data[idx]
                    else:
                        for idx in range(data_len):
                            sum_data += obj_hist.ft_data[idx]

                    ave_data = sum_data / data_len
                    if data_len > 0:
                        tmp[3] = data_len
                        tmp[4] = "%06d" % obj_hist.it_time[0]
                        tmp[5] = "%06d" % obj_hist.it_time[data_len - 1]
                    #
                    if alarm_len > 0:
                        tmp[6] = alarm_len
                        tmp[7] = "%06d" % obj_hist.it_alarm_time[0]
                        tmp[8] = "%06d" % obj_hist.it_alarm_time[alarm_len - 1]
                    tmp[9]  = float("%.1f"%ave_data)
                    tmp[10] = float("%.1f"%max_data)
                    tmp[11] = float("%.1f"%min_data)
                items.append(tmp)
                #logging.error('%s::HistoryList::shop_id=%s, point_id=%s, ym=%s' %(ctrl_name, shop_id, item.point_id, start_date ))

            lang_sel = loginData['shop_user'].lang_sel
            template_vals = {
                    'lang':lang_sel,
                    'control':translate('report', lang_sel),
                    'title':"POINT詳細",
                    'loginData':loginData,
                    'shop_id': shop_id,
                    'shops': shops,
                    'shop':obj_shop,
                    'start_date': start_date,
                    'obj_point':obj_point,
                    'week_name':week_name,
                    'point_id': point_id,
                    'limit_up': float("%.1f"%obj_point.limit_up),
                    'limit_dn': float("%.1f"%obj_point.limit_dn),
                    'items': items,
                    'sel_period':4,  #1：FromTo(Day)　2：FromTo(Month), 3：Day 4:Month, 0:期間表示いない。 -9:条件バー表示しない。
                }
            self.helpDispatch("primary", 'report_point_month.html', template_vals)
        except UserException as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.value)
        except Exception as e:
            occur_point = "%s::HistoryList::"%ctrl_name
            func_error_display(self, occur_point + e.message)
            raise


app = webapp.WSGIApplication([
    (slash_dir,  HistoryListDay),
    (slash_dir + "list_day",    HistoryListDay),
    (slash_dir + "point_day",   HistoryPointDay),
    (slash_dir + "list_month",  HistoryListMonth),
    (slash_dir + "point_month", HistoryPointMonth),
    #(slash_dir + "year",  YearReport),
    ], debug=True)
