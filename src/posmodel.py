# -*- coding: utf-8 -*-
import os
import logging
import datetime
import calendar
import ConfigParser
import binascii

from datetime import date
from calendar import weekday

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import memcache

G_SERVER_KUBUN = ""
G_LANG_SEL="jp"

'''
Local DB Clear
cd Local Dir
dev_appserver.py --clear_datastore src
'''
def change_utf8n(self):
    bitlist = ['EF', 'BB', 'BF']
    bytes = binascii.a2b_hex(''.join(bitlist))
    self.response.out.write(bytes)

def currency(money_int):
    if not money_int:
        money_int = 0
    if G_LANG_SEL == "jp" or G_LANG_SEL == "kr":
        return money_int
    if G_LANG_SEL == "zh":
        return "%d.%02d"%(int(money_int / 100), (money_int % 100))

def currency_form(money_int, blank_ok):
    if not money_int:
        if blank_ok:
            money_int = 0
        else:
            return ""
    if G_LANG_SEL == "jp":
        money_int = int(money_int)
        return "¥{:,d}".format(money_int)
    if G_LANG_SEL == "kr":
        money_int = int(money_int)
        return "₩{:,d}".format(money_int)
    if G_LANG_SEL == "zh":
        return "¥{:,.2f}".format(money_int / 100.0)

#小数点以下２桁まで許容
def int_currency(money_string):
    if not money_string:
        return 0
    if G_LANG_SEL == "jp" or G_LANG_SEL == "kr":
        return int(money_string)
    if G_LANG_SEL == "zh":
        return int(float(money_string) * 100)
'''
'''
def translate(keyword, langs="jp", reload=False):
    mem_key = "LANGS/%s"%langs
    if not langs: langs = G_LANG_SEL
    #
    def readIni(langs):
        config = ConfigParser.ConfigParser()
        fileObj = None
        try:
            fileObj = open('langs.' + langs)
        except Exception as e:
            try:
                fileObj = open('langs.jp')
            except Exception as e:
                logging.error('translate not found LanguageFile = ' + langs)
        config.readfp(fileObj)
        data = config._sections
        memcache.Client().set(key = mem_key, value = data, time = 6000)
        return data
    #
    def readKeyword(data, langs, keyword):
        try:
            tr = data[langs][keyword]
            return tr
        except KeyError:
            return None
    #----
    data = None
    if reload:
        data = readIni(langs)
    else:
        #data = readIni(langs) #本番ではコメントアウトすること。
        data = memcache.Client().get(mem_key)
        if data == None:
            data = readIni(langs)
    tr = readKeyword(data, langs, keyword)
    if not tr:
        if langs != 'jp':
            tr = readKeyword(data, 'jp', keyword)
            if not tr: tr = keyword
        else:
            tr = keyword
    return tr

def trans_unicode(keyword, langs="jp", reload=False):
    tr = translate(keyword, langs, reload)
    return unicode(tr, 'utf-8')

def func_get_fromto_month(self, obj_shop):
    start_date = self.request.get('startDate').replace("/", "")
    if not start_date:
        now = getJST() #
        start_date = now.strftime("%Y%m")
    end_date = self.request.get('endDate').replace("/", "")
    if not end_date:
        now = getJST()
        end_date = now.strftime("%Y%m")
    return start_date, end_date

def func_get_fromto_day(self, obj_shop):
    start_date = self.request.get('startDate').replace("/", "")
    if not start_date:
        now = getJST() #
        start_date = now.strftime("%Y%m%d")
    end_date = self.request.get('endDate').replace("/", "")
    if not end_date:
        now = getJST()
        end_date = now.strftime("%Y%m%d")
    return start_date, end_date


def func_get_common_shopinfo(self, loginData):
    shop_id = self.request.get('shop_id')
    if not shop_id:
        shop_id = self.request.get('sel_shop_id')
        if not shop_id:
            shop_id  = loginData['shop_user'].shop_id
    obj_shop = get_mem_shop(shop_id)
    if not obj_shop:
        return None, None, None
    loginData['shop_info'] = obj_shop

    shops=[]
    #
    if loginData['shop_user'].role_flag < 2: #POS、店長（店舗指定）
        shops.append(loginData['shop_info'])
    else:
        obj_comp = obj_shop.company_key
        if obj_comp:
            for item in obj_comp.shopinfo_set:
                if item.flag_del: continue
                shops.append(item)

        if shop_id != loginData['shop_user'].shop_id:
            loginData['shop_user'].shop_id = shop_id
            loginData['shop_user'].put()
            userKey = "user/" +  loginData['shop_user'].email_id
            memcache.Client().delete(userKey)
    #
    group_id = self.request.get('sel_group_id')
    if group_id > "0":
        #logging.error('3333posmodel::func_get_common_shopinfo::return shop_id=%s' % (group_id))
        return group_id, obj_shop, shops
    #
    #logging.error('4444posmodel::func_get_common_shopinfo::return shop_id=%s' % (shop_id))
    return shop_id, obj_shop, shops


class UserException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def func_error_display(self, param):
    template_vals = {
            'title':"エラー内容表示",
            'error':param,
        }
    logging.error('Exception ERROR Message = ' + param)
    self.helpDispatch('init', 'errordisp.html', template_vals)
    return

def getCalculateAgoDate(para_day=0):
    time = datetime.datetime.now()
    return time - datetime.timedelta(days = para_day)

def getJST(time = None):
    if not time:
        time = datetime.datetime.now()
    if G_LANG_SEL == "zh":
        return  time + datetime.timedelta(hours=8)
    return time + datetime.timedelta(hours=9)

def getJSTplus11(): #ForChina
    return datetime.datetime.now() + datetime.timedelta(hours=11)

def getYearList(obj_shop):
    year_list = []
    start_year = int(obj_shop.start_ymd[:4])
    cur_date = datetime.datetime.now()
    for idx in range(7):
        year_list.append(u"%04d"%(cur_date.year - idx))
        if start_year == cur_date.year - idx: break
    return year_list

def getMinMonth(obj_shop, min_month):
    dt = datetime.datetime.strptime(obj_shop.start_ymd, '%Y%m%d')
    cur_date = datetime.datetime.now()
    tmp_year  = cur_date.year  - dt.year
    tmp_month = cur_date.month - dt.month + 1
    diff_month = tmp_year * 12 + tmp_month
    #logging.error('posmodel::getMinMonth::tmp_year=%d, diff_month=%d' % (tmp_year, diff_month))
    if min_month > diff_month:
        return "-%dM"%(diff_month)
    return "-%dM"%(min_month)


def get_mem_servername():
    serverName =  "server_name"
    data = memcache.Client().get(serverName)
    if data == None:
        data = os.environ['HTTP_HOST']
        memcache.Client().set(key = serverName, value = data)
    return data

def get_mem_company(company_id):
    companyKey =  "company/" + company_id
    data = memcache.Client().get(companyKey)
    if data == None:
        data = CompanyInfo.get_by_key_name(company_id)
        if data == None:
            logging.error('posmodel::get_mem_company::not found company=' + company_id)
            return None
        memcache.Client().set(key = companyKey, value = data, time = 300)
    return data

def del_mem_company(company_id):
    companyKey =  "company/" + company_id
    memcache.Client().delete(companyKey)
    return

def get_mem_user(user):
    '''memcache get_mem_user
    @param user:
    @return: ShopUser Record
    '''
    emailString  = user.email()
    email_id = emailString[:emailString.find("@")]

    userKey =  "user/" + email_id
    data = memcache.Client().get( userKey )
    if data == None:
        data = ShopUser.get_by_key_name(email_id)
        if data == None:
            logging.debug('posmodel::get_mem_user::not found user=[%s]' % email_id)
            return None
        memcache.Client().set(key = userKey, value = data, time = 300)
    return data
def del_mem_user(user):
    emailString  = user.email()
    email_id = emailString[:emailString.find("@")]
    userKey =  "user/" + email_id
    memcache.Client().delete(userKey)
    return

def get_mem_shop(shop_id):
    '''memcache get_mem_shop
    @param shop_id: company_id(6桁) + SEQ(2桁)
    @return: ShopInfo Record
    '''
    mem_shop_key =  "shop/" + shop_id
    data = memcache.Client().get(mem_shop_key)
    if data == None:
        data = ShopInfo.get_by_key_name(shop_id)
        if data == None:
            logging.error('posmodel::get_mem_shop::not found shop=' + shop_id)
            return None
        memcache.Client().set(key = mem_shop_key, value = data, time = 300)
    return data

def del_mem_shop(shop_id):
    shopKey =  "shop/" + shop_id
    memcache.Client().delete(shopKey)
    return

'''
'''
def get_mem_section(section_key):
    data = SectionTable.get_by_key_name(section_key)
    if data == None:
        if section_key[8:] == "99": return None
        logging.error('posmodel::get_mem_section::not found section=' + section_key)
        return None
    return data

def get_mem_product(product_key):
    data = PointTable.get_by_key_name(product_key)
    if data == None:
        logging.error('posmodel::get_mem_product::not found product=' + product_key)
        return None
    return data

'''
'''
def get_mem_calendar(month_key):
    data = CalendarData.get_by_key_name(month_key)
    if data == None:
        logging.error('posmodel::get_mem_calendar::not found month=' + month_key)
        return None
    return data

def del_mem_calendar(month_key):
    return


###################################################
# Common Module
###################################################
class ApplicationHelper():
    def render(self, group, template):
        return os.path.join(os.path.dirname(__file__), 'views/' + group, template) # テンプレートのOSファイル名を返す

class Helper(webapp.RequestHandler, ApplicationHelper):
    def helpDispatch(self, template_group, template_name, vals): # ディスパッチ用のヘルパーメソッド
        self.response.out.write(template.render(\
            self.render(template_group, template_name), vals)) # 指定されたテンプレートをレンダリングするメソッド

    def logincheck(self, flag=0):
        '''logincheck
        @param :
        @return: loginData
        '''
        logging.getLogger().setLevel(logging.DEBUG)
        user = users.get_current_user()
        if not user:
            template_vals = {
                    'title':"ログイン失敗",
                    'loginData':None,
                }
            logging.debug('login error!!! ')
            if flag==0:
                self.helpDispatch('init', 'unlogined.html', template_vals)
            return

        loginData = {'url':users.create_logout_url("/"),
                     'user':user,
                     'shop_user':None,
                     'shop_info':None,
                     'url_linktext':'Logout'}

        objShopUser = get_mem_user(user)
        if not objShopUser:
            template_vals = {
                    'title':"未登録ユーザ",
                    'loginData':loginData,
                }
            if flag==0:
                logging.debug('unregistered user!!! user=' + user.email())
                self.helpDispatch('init', 'unregistered.html', template_vals)
            else:
                if flag==1: #AJAX
                    logging.debug('unregistered user ajax!!! user=' + user.email())
            return

        loginData['shop_user'] = objShopUser
        loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)
        #G_LANG_SEL = loginData['shop_user'].lang_sel

        return loginData


    def logincheck_demo(self, flag=0):
        '''logincheck
        @param :
        @return: loginData
        '''
        logging.getLogger().setLevel(logging.DEBUG)
        objShopUser = None

        user = users.get_current_user()
        if user:
            objShopUser = get_mem_user(user)
        else:
            objShopUser = ShopUser.get_by_key_name("berutio")

        loginData = {'url':users.create_logout_url("/"),
                     'user':user,
                     'shop_user':None,
                     'shop_info':None,
                     'url_linktext':'Logout'}
        loginData['shop_user'] = objShopUser
        loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)
        return loginData

'''
#2
'''
class CompanyInfo(db.Model):
    company_id   = db.StringProperty()   #KEYNAME(6degit serialno)
    kname        = db.StringProperty(indexed=False)    #ShopName
    zipcode      = db.StringProperty(indexed=False)    #郵便番号
    address1     = db.StringProperty(indexed=False)    #住所１
    address2     = db.StringProperty(indexed=False)    #住所２
    address3     = db.StringProperty(indexed=False)    #住所3
    telno        = db.StringProperty(indexed=False)    #電話番号
    faxno        = db.StringProperty(indexed=False)    #FAX番号
    lastno_shop  = db.IntegerProperty(indexed=False)   #店舗最後番号 (店舗数が１個しかない会社の区分にも使われる）
    admin_id     = db.StringProperty(indexed=False)   #管理者
    start_ymd    = db.StringProperty(indexed=False)   #使用開始年月(YYYYMMDD)

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["company_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.lastno_shop  = 0
        entity.lastno_siire = 0
        d = getJST()
        entity.start_ymd = "%04d%02d%02d"%(d.year, d.month, d.day) #
        entity.put()
        return entity

class ShopInfo(db.Model):
    company_key = db.ReferenceProperty(reference_class=CompanyInfo)
    shop_id     = db.StringProperty()   #8degit
    kname       = db.StringProperty(indexed=False)    #ShopName
    zipcode     = db.StringProperty(indexed=False)    #郵便番号
    address1    = db.StringProperty(indexed=False)    #住所１
    address2    = db.StringProperty(indexed=False)    #住所２
    address3    = db.StringProperty(indexed=False)    #住所3
    telno       = db.StringProperty(indexed=False)    #電話番号
    faxno       = db.StringProperty(indexed=False)    #FAX番号
    admin_id    = db.StringProperty(indexed=False)    #管理者
    start_ymd   = db.StringProperty(indexed=False)    #使用開始年月(YYYYMMDD)
    flag_del    = db.BooleanProperty(indexed=False)   #True:Deleted
    #
    update_date = db.StringProperty(indexed=False) #入力日

    @classmethod
    def insert_or_fail(cls, **kwds):
        obj_comp = get_mem_company(kwds["company_id"])
        if not obj_comp: return
        obj_comp.lastno_shop += 1
        obj_comp.put()
        del_mem_company(obj_comp.company_id)

        key_name = obj_comp.company_id + "%02d"%(obj_comp.lastno_shop)
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.company_key = obj_comp
        entity.shop_id     = key_name
        d = getJST()
        entity.start_ymd = "%04d%02d%02d"%(d.year, d.month, d.day) #
        entity.put()

        ''' #ShopUser追加
        '''
        if 'role_flag' not in kwds:
            role_flag = 1
        else:
            role_flag = kwds["role_flag"]
        if entity.admin_id:
            ShopUser.insert_or_fail(
                            shop_id     = entity.shop_id,
                            email_id    = entity.admin_id,
                            role_flag   = role_flag
                          )
        return entity

'''
#5. 部門マスタ
'''
class SectionTable(db.Model):
    shop_key    = db.ReferenceProperty(reference_class=ShopInfo)
    shop_id     = db.StringProperty()  #KEY 8degit
    sect_id     = db.StringProperty()  #KEY 2degit 部門コード
    food_drink  = db.IntegerProperty(indexed=False)  #1:drink, 2:food
    kname       = db.StringProperty(indexed=False)     #部門名（漢字）
    lastno_prod = db.IntegerProperty(indexed=False) #商品番号
    priority    = db.IntegerProperty(indexed=False) #Default = 50
    image_no    = db.IntegerProperty(indexed=False) #Default = 50
    multi_tax   = db.IntegerProperty(indexed=False) #5:軽減税率
    common_sect = db.IntegerProperty(indexed=False) #会社共通商品
    flag_del    = db.BooleanProperty(indexed=False) #True:Deleted
    flag_stock  = db.BooleanProperty(indexed=False) #True:
    flag_discount = db.BooleanProperty(indexed=False) #True:Discount可

    @classmethod
    def insert_or_fail(cls, **kwds):
        obj_shop = get_mem_shop(kwds["shop_id"])
        sect_id = "%02d"%obj_shop.lastno_sect
        key_name = kwds["shop_id"] + sect_id
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            #logging.error('posmodel::ProductTable::already exist...then,+1 =' + key_name)
            for last_no in range(obj_shop.lastno_sect + 1, 99):
                obj_shop.lastno_sect += 1
                sect_id = "%02d"%obj_shop.lastno_sect
                key_name = kwds["shop_id"] + sect_id
                entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
                if not entity:
                    break
            if entity:
                logging.error('posmodel::SectionTable::can not append, already exist=' + key_name)
                return None

        entity = cls(key_name=key_name, **kwds)
        entity.shop_key = obj_shop
        entity.sect_id  = sect_id
        entity.image_no = int(sect_id) % 20
        #
        if 'common_sect' not in kwds:
            entity.common_sect = 0
        if 'lastno_prod' not in kwds:
            entity.lastno_prod = 0
        if 'priority' not in kwds:
            entity.priority = 50
        entity.lastno_prod += 1
        if 'multi_tax' not in kwds:
            entity.multi_tax = 0
        entity.put()
        #
        obj_shop.lastno_sect += 1
        obj_shop.put()
        del_mem_shop(kwds["shop_id"])
        obj_shop = get_mem_shop(kwds["shop_id"])
        return entity

'''
#9. ログインユーザ
'''
class ShopUser(db.Model):
    company_key = db.ReferenceProperty(reference_class=CompanyInfo, indexed=False)
    email_id    = db.StringProperty()   #KEY
    shop_id     = db.StringProperty()   #
    role_flag   = db.IntegerProperty(indexed=False)  #0:店舗Staff, 1:店長, 2:店長＋他店照会, 9:本部
    regi_no     = db.IntegerProperty(indexed=False)  #0:default, 1:２番レジ、2:3番レジ....
    regi_type   = db.IntegerProperty(indexed=False)  #0:default, 1:物販, 2:biliad向け（ホール伝票が最初のみ出力）、3:Slaveとして使う 4:新物販
    subregi_tab = db.IntegerProperty(indexed=False)  #0:default, 1:２番ショップ、2:３番ショップ
    disabl_edit = db.IntegerProperty(indexed=False)  #0 or Null:全ての権限、1:修正権限なし（登録ボタン表示しない）
    #enable_limit_time = db.IntegerProperty(indexed=False)  #0 or Null:ShopInfoのtable_limit_timeを使わない。、1:使う
    up_stop     = db.BooleanProperty(indexed=False) #True:アップロード禁止
    group_no    = db.IntegerProperty(indexed=False)  #0 or Null:全ての店舗、その他：グループ番号
    lang_sel    = db.StringProperty(indexed=False)  #

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["email_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        obj_comp = get_mem_company(kwds["shop_id"][:6])
        entity.company_key = obj_comp
        if 'disabl_edit' not in kwds:
            entity.disabl_edit = 0
        if 'regi_no' not in kwds:
            entity.regi_no = 0
        if 'regi_type' not in kwds:
            entity.regi_type = 0
        entity.lang_sel  = "jp"
        entity.put()
        return entity


'''
#=========================================================
# 18. カレンダー
# 予約テーブル　＋　シフト　共有
# 最大４ヶ月のデータを持たせる。
# 1.Shop作成時に当月含めて４ヶ月作成
# 2.毎月１日に４ヶ月後のデータ作成
#=========================================================
'''
class CalendarData(db.Model):
    shop_id      = db.StringProperty()  #KEY 8degit
    year_month   = db.StringProperty()  #KEY 8degit YYYYMMDD（予約日）
    it_work_from = db.ListProperty(int, indexed=False) #営業開始時間　From==To --> 休日
    it_work_to   = db.ListProperty(int, indexed=False) #営業終了時間
    it_rsv_from  = db.ListProperty(int, indexed=False) #予約開始時間
    it_rsv_to    = db.ListProperty(int, indexed=False) #予約終了時間　※To時間は２４以下又は営業終了時間
    it_rsv_max   = db.ListProperty(int, indexed=False) #最大予約人数
    it_rsv_mans  = db.ListProperty(int, indexed=False) #予約人数
    it_rsv_cnts  = db.ListProperty(int, indexed=False) #予約件数
    #-----SHIFT DATA--2013/1/25
    shift_flag     = db.IntegerProperty(indexed=False)   #0:シフト指定なし、1:会社仮, 2:個人申請有, 3:会社調整確定
    sime_date      = db.StringProperty(indexed=False)    #個人更新申請の締め日(YYYYMMDD)
    kakutei_date   = db.StringProperty(indexed=False)    #店舗のシフト確定日(YYYYMMDD)
    it_sft_mancnt  = db.ListProperty(int, indexed=False) #勤務人数（合計）
    it_sft_manover = db.ListProperty(int, indexed=False) #過不足有無（-1:不足, 0:正常, 1:超過
    it_sft_change  = db.ListProperty(int, indexed=False) #変更申請があったか（０：なし、１：あり）

    @classmethod
    def get_or_initialize(cls, **kwds):
        key_name = kwds["shop_id"] + kwds["year_month"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity: return entity

        last_day = calendar.monthrange(int(kwds["year_month"][:4]), int(kwds["year_month"][4:]))[1]
        entity = cls(key_name=key_name, **kwds)
        entity.it_work_from = [kwds["time_open"]      for j in range(last_day)]
        entity.it_work_to   = [kwds["time_close"]     for j in range(last_day)]
        entity.it_rsv_from  = [kwds["rsv_start_time"] for j in range(last_day)]
        entity.it_rsv_to    = [kwds["rsv_end_time"]   for j in range(last_day)]
        entity.it_rsv_max   = [kwds["rsv_max_man"]    for j in range(last_day)]
        entity.it_rsv_mans  = [0 for j in range(last_day)]
        entity.it_rsv_cnts  = [0 for j in range(last_day)]
        #-----SHIFT DATA--2013/1/25
        entity.shift_flag     = 0    #0:シフト指定なし、1:会社仮, 2:個人申請有, 3:会社調整確定
        entity.it_sft_mancnt  = [0 for j in range(last_day)]
        entity.it_sft_manover = [0 for j in range(last_day)]
        entity.it_sft_change  = [0 for j in range(last_day)]
        #
        entity.put()
        return entity


    @classmethod
    def initialize(cls, **kwds):
        key_name = kwds["shop_id"] + kwds["year_month"]
        last_day = calendar.monthrange(int(kwds["year_month"][:4]), int(kwds["year_month"][4:]))[1]
        entity = cls(key_name=key_name, **kwds)
        entity.it_work_from = [kwds["time_open"]      for j in range(last_day)]
        entity.it_work_to   = [kwds["time_close"]     for j in range(last_day)]
        entity.it_rsv_from  = [kwds["rsv_start_time"] for j in range(last_day)]
        entity.it_rsv_to    = [kwds["rsv_end_time"]   for j in range(last_day)]
        entity.it_rsv_max   = [kwds["rsv_max_man"]    for j in range(last_day)]
        entity.it_rsv_mans  = [0 for j in range(last_day)]
        entity.it_rsv_cnts  = [0 for j in range(last_day)]
        #-----SHIFT DATA--2013/1/25
        entity.shift_flag     = 0    #0:シフト指定なし、1:会社仮, 2:個人申請有, 3:会社調整確定
        entity.it_sft_mancnt  = [0 for j in range(last_day)]
        entity.it_sft_manover = [0 for j in range(last_day)]
        entity.it_sft_change  = [0 for j in range(last_day)]
        #
        entity.put()
        return entity


class UnregistedUserHistory(db.Model):
    user_id     = db.StringProperty()
    dw_count    = db.IntegerProperty(indexed=False) # 監査区分コード
    update_date = db.DateTimeProperty(indexed=False, auto_now=True) #更新日

    @classmethod
    def insert_or_update(cls, **kwds):
        key_name = kwds["user_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if not entity:
            entity = cls(key_name=key_name, **kwds)
            entity.dw_count = 0
        #
        entity.dw_count += 1
        entity.put()
        return entity



class SystemCalendarData(db.Model):
    year_month = db.StringProperty()  #KEY 8degit YYMM
    holiday    = db.ListProperty(int, indexed=False) #0:平日, 1:Holiday

    @classmethod
    def get_or_initialize(cls, **kwds):
        key_name = kwds["year_month"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if not entity:
            entity = cls(key_name=key_name, **kwds)
            entity.holiday = [0 for j in range(31)]
            #
            tmp = calendar.monthrange(int(key_name[:4]), int(key_name[4:6]))
            week_first = tmp[0]
            entity.holiday  = [0 for j in range(31)]
            for idx in range(31):
                week = (week_first + idx + 1)%7
                if week == 6 or week == 0:
                    entity.holiday[idx] = 1
            entity.put()
        #
        return entity


'''
#7. POINT マスタ
１．当分は部門を考えなく、POINTのみを管理する。
'''
class PointTable(db.Model):
    shop_id   = db.StringProperty()  #
    sect_id   = db.StringProperty()  # 3degit 部門コード(MultiGW번호)
    point_id  = db.StringProperty()  # POINT번호(MultiGW번호 3자리 + RTD번호 2자리)
    kname     = db.StringProperty(indexed=False)  # ディバイス名
    limit_up  = db.FloatProperty(indexed=False)   # 上限
    limit_dn  = db.FloatProperty(indexed=False)   # 下限
    location  = db.StringProperty(indexed=False)  # 設置場所
    comment   = db.StringProperty(indexed=False)  # COMMENT
    start_ymd = db.StringProperty(indexed=False)  # 使用開始日
    interval  = db.IntegerProperty(indexed=False) # Interval(SEC)
    flag_del  = db.BooleanProperty(indexed=False) # True:Deleted
    auto_update_date = db.DateTimeProperty(auto_now=True, indexed=False) #更新日

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["shop_id"] + kwds["point_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            #logging.debug('posmodel::PointTable::can not append, already exist=' + key_name)
            return entity
        entity = cls(key_name=key_name, **kwds)
        if not entity.sect_id:  entity.sect_id  = "001"
        if not entity.kname:    entity.kname    = ""
        if not entity.limit_up: entity.limit_up = 50
        if not entity.limit_dn: entity.limit_dn = 0
        if not entity.location: entity.location = ""
        if not entity.interval: entity.interval = 10
        if not entity.comment:  entity.comment  = ""
        if not entity.start_ymd:
            d = getJST()
            entity.start_ymd = "%04d%02d%02d"%(d.year, d.month, d.day) #
        entity.put()
        return entity

import random #For Test
'''
#History....
１．当分は部門を考えなく、履歴のみを管理する。
1Record =  two most obvious limits are the 1MB maximum entity size and 20000 index entries.
INTERVALはMIN 10秒にする。
'''
class TloggerHistory(db.Model):
    shop_id  = db.StringProperty()  #
    ymd      = db.StringProperty()  # YYMMDD KEY2
    point_id = db.StringProperty()  # point_id KEY1
    ft_data  = db.ListProperty(float, indexed=False) #10초별온도값: Float List	360 * 24 = 8640
    it_time  = db.ListProperty(int, indexed=False)   #時分秒        : INT List   360 * 24 = 8640
    ft_alarm_data = db.ListProperty(float, indexed=False)
    it_alarm_time = db.ListProperty(int, indexed=False)   #時分秒6桁
    last_time = db.IntegerProperty(indexed=False)  #最終受付データの時刻：この時間以降のデータを受け取る

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = "%s%s%s" % (kwds["shop_id"], kwds["point_id"], kwds["ymd"])
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.put()
        return entity

class TloggerHistoryMonth(db.Model):
    shop_id  = db.StringProperty()  #
    ym       = db.StringProperty()  # YYMM KEY2
    point_id = db.StringProperty()  # point_id KEY1
    it_data_cnt  = db.ListProperty(int, indexed=False)   # 収集データ件数
    it_alarm_cnt = db.ListProperty(int, indexed=False)   # アラムデータ件数
    ft_ave_data  = db.ListProperty(float, indexed=False)   # AVE
    ft_min_data  = db.ListProperty(float, indexed=False)   # MIN
    ft_max_data  = db.ListProperty(float, indexed=False)   # MAX

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = "%s%s%s" % (kwds["shop_id"], kwds["point_id"], kwds["ym"])
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if not entity:
            entity = cls(key_name=key_name, **kwds)
        if not entity.it_data_cnt:  entity.it_data_cnt  = [0 for j in range(31)]
        if not entity.it_alarm_cnt: entity.it_alarm_cnt = [0 for j in range(31)]
        if not entity.ft_ave_data:  entity.ft_ave_data  = [0.0 for j in range(31)]
        if not entity.ft_min_data:  entity.ft_min_data  = [0.0 for j in range(31)]
        if not entity.ft_max_data:  entity.ft_max_data  = [0.0 for j in range(31)]
        entity.put()
        return entity
