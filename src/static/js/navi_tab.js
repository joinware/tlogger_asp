﻿(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-42719073-1');
ga('send', 'pageview');

$(function(){
	$(".lnb_wrap").hide();
	$(".gnb_wrap .gnb > li").each(function(num) {
		var nowDs = $(".gnb_wrap .gnb > li").filter(".current").index();
		var navWrap = $(".gnb_wrap .gnb > li");
		var navNum = $(".gnb_wrap .gnb > li .gnb_name");
		var navSub = $(".gnb_wrap .gnb > li .gnb_list");
		$(".gnb_list").css("background","none");
		$(".gnb").css("padding-top","10px").parent().css("top","63px").end().find(".gnb_list").css("top","53px");
		$(this).mouseenter(function() {
			for(var i = 0; i < navNum.length; i++) {
				if(i == num) {
					navNum.eq(i).find("img").attr("src", navNum.eq(i).find("img").attr("src").replace("_off","_on"));
					navWrap.eq(i).addClass("current");
					navSub.eq(i).find("ul").stop(true, false).animate({top:"0"}, 300);
				}
				else {
					navNum.eq(i).find("img").attr("src", navNum.eq(i).find("img").attr("src").replace("_on","_off"));
					navSub.eq(i).find("ul").stop(false, true).animate({top:"-43px"}, 300);
					navWrap.eq(i).removeClass("current");
				}
			}
		}).parent().mouseleave(function() {
			for(var i = 0; i < navNum.length; i++) {
				if(i == nowDs) {
						navNum.eq(i).find("img").attr("src", navNum.eq(i).find("img").attr("src").replace("_off","_on"));
						navWrap.eq(i).addClass("current");
						navSub.eq(i).find("ul").stop(true, false).animate({top:"0"}, 200);
				}
				else {
						navNum.eq(i).find("img").attr("src", navNum.eq(i).find("img").attr("src").replace("_on","_off"));
						navSub.eq(i).find("ul").stop(false, true).animate({top:"-43px"}, 200);
						navWrap.eq(i).removeClass("current");
				}
			}
		});
	});

	$(".gnb_list ul li a img").mouseenter(function() {
		$(this).attr("src", $(this).attr("src").replace("_off","_on"));
	}).mouseleave(function() {
		$(this).attr("src", $(this).attr("src").replace("_on","_off"));
	});

	$(".tab_area .tab_btn a").each(function(num) {
		var tabNum = $(".tab_area .tab_btn li");
		$(this).click(function(e) {
			for(var i = 0; i < tabNum.length; i++) {
				if(i == num) {
					tabNum.eq(i).find("img").attr("src",tabNum.eq(i).find("img").attr("src").replace("_off.gif","_on.gif")).end();
					$(".tab_area .tab_box").eq(i).css("display","block");
				}
				else {
					tabNum.eq(i).find("img").attr("src",tabNum.eq(i).find("img").attr("src").replace("_on.gif","_off.gif")).end();
					$(".tab_area .tab_box").eq(i).css("display","none");
				}
			}
			e.preventDefault();
		});
	});
	
	//$(".lnb li:eq(1) a").attr("href","jav"+"ascript:alert('준비중입니다.')").click(function(){
	//	e.preventDefault();
	//});
	
	
});