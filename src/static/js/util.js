function rgb2hex(rgb){
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 return "#" +
  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
}

function blurTextarea(obj){
	$(".touch_product_selected").children(".touch_text").html($.nl2br(obj.value));
	$(".product_edit").remove();
}

jQuery.nl2br = function(varTest){
	return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
};

jQuery.br2nl = function(varTest){
	return varTest.replace(/<br>/g, "\r");
};

function getLines(obj)
{
	return obj.val().split("\n").length
}

function checkLineNum(){
	var allowedNumberOfLines = 1;
	if(getLines($(".product_edit")) > allowedNumberOfLines)
	{
		  modifiedText = $(".product_edit").val().split("\n").slice(0, allowedNumberOfLines);
		  $(".product_edit").val(modifiedText.join("\n"));
	}
}

function ajaxCall(target, msgJson, callback){
	url = "/ajax/"+target;
	data = {
        "msg":msgJson,
        "master":target
	};
    $.post(url, data, callback).fail(function() { alert("サーバとの通信に異常があります。ネットワークをチェックして下さい。"); });
}


function string_json(arr){
	var len=arr.length;
	var jtext="{";
	for(i=0;i<len-1;i++){
		jtext+="'"+$("#"+arr[i]).attr("name")+"':'"+encodeURI($("#"+arr[i]).val())+"',";
	}
	jtext+="'"+$("#"+arr[len-1]).attr("name")+"':'"+encodeURI($("#"+arr[len-1]).val())+"'";
	jtext+="}";

	return jtext;
}

function array2json(arr){
	var len=arr.length;
	var jtext="{";
	for(var i in arr){
		if(typeof(arr[i])=="object"){
			jtext+="'"+i+"':["+arr[i]+"],";
		}else{
			jtext+="'"+i+"':'"+arr[i]+"',";
		}
	}
	jtext=jtext.substr(0,jtext.length-1);
	jtext+="}";
	return jtext;
}

function wrapWindowByMask(){
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    $('#mask').fadeIn(0);
}

function disableMask(){
    $('#mask').hide();
}

function makeJsonData(){
	var arrJson  = new Array();
	$(".ajax_datas").each(function(){
		arrJson.push($(this).attr("name"));
	});
	return string_json(arrJson);
}

function findVal(array,id){
	for ( var int = 0; int < array.length; int++) {
		if (array[int][0] == id) {
			return array[int][1];
		}
	}
	return "";
}

function colorCode(hex){
	if (hex<10) {
		if (hex==1||hex==0) {
			return "#dcdcdc";
		}else if (hex==2) {
			return "#a8baff";
		}else if (hex==3) {
			return "#fffaa8";
		}else if (hex==4) {
			return "#ffa8ae";
		}else if (hex==5) {
			return "#b3ffa8";
		}
	}else {
		if (hex=="#dcdcdc") {
			return 1;
		}else if (hex=="#a8baff") {
			return 2;
		}else if (hex=="#fffaa8") {
			return 3;
		}else if (hex=="#ffa8ae") {
			return 4;
		}else if (hex=="#b3ffa8") {
			return 5;
		}
	}
}

function checkLength(obj,limit){
	var len=obj.value.length;
	if (len>=limit) {
		obj.value=obj.value.substr(0,limit-1);
	}
}

function numberCheck(obj){
	if (isNaN(obj.value)==false) {
		if ($.isNumeric(obj.value)==false) {
			//false
			alert(onlyNumber);
			$(obj).val("");
		}
	}else {
		//false
		alert(onlyNumber);
		$(obj).val("");
	}
}

function limitLineTextarea(obj,limit){
	var msgArray =new Array();
	var msg = ""+$(obj).val();
	msgArray = msg.split("\n");
	if (msgArray.length>=limit) {
		var j=0;
		if (msgArray[0].length==0) {
			j=1;
		}
		$(obj).val(msgArray[j]+"\n");
		for ( var int = j+1; int < msgArray.length; int++) {
			$(obj).val($(obj).val()+msgArray[int]);
		}
		$(obj).blur();
	}
}

function number_format(nStr,flag){
	nStr=$.trim(nStr);
	var minusChar="";
	var yenChar="";
	var perChar="";
	if(nStr==""){
		nStr=0;
	}
	// flag-1 yen
	if (flag==1 || flag==4) {
		yenChar="\u00a5";
		nStr=Round(nStr,0);
	}else if(flag==2){
		nStr=eval(nStr);
		nStr=Round(nStr,1);
		perChar="%";
	}else {
		nStr=Round(nStr,0);
	}

	if (nStr<0) {
		nStr = nStr.replace("-","");
		minusChar="▲";
	}

	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	var ret_str = minusChar + yenChar + x1 + x2 + perChar;
	if (flag == 4 && x1 == "0") 	return "";
	if (flag == 5 && x1 == "0") 	return "";
	return ret_str;

}


function Round(n, pos) {
	var digits = Math.pow(10, pos);

	var sign = 1;
	if (n < 0) {
		sign = -1;
	}
	n = n * sign;
	var num = Math.round(n * digits) / digits;
	num = num * sign;

	return num.toFixed(pos);
}


function printSection()
{
	 var sw = screen.witdh;
	 var sh = screen.height;
	 var w = 1200;
	 var h = 800;
	 var xpos = (sw-w)/2;
	 var ypos = (sh-h)/2;
	 var rptHeader = " <html><head><title>印刷 </title> ";

	 rptHeader = rptHeader + '<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />';
	 rptHeader + "</head><body>";
	 rptContent0 = $(".print").html();
	 var rptTail = "</body></html>";
	 var rptContent = rptHeader + rptContent0 + rptTail;
	 rptWin = window.open( "","rpt","witdh="+w+",height="+h+",top="+ypos+",left="+xpos+",status=yes,scrollbars=yes");
	 rptWin.document.open();
	 rptWin.document.write(rptContent);
	 rptWin.document.close();
	 rptWin.print();
	 rptWin.close();
}

function printOne()
{
	 var sw = screen.witdh;
	 var sh = screen.height;
	 var w = 1200;
	 var h = 800;
	 var xpos = (sw-w)/2;
	 var ypos = (sh-h)/2;
	 var rptHeader = " <html><head><title>印刷 </title> ";

	 rptHeader = rptHeader + '<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />';
	 rptHeader + "</head><body>";
	 rptContent0 = $(".printOne").html();
	 var rptTail = "</body></html>";
	 var rptContent = rptHeader + rptContent0 + rptTail;
	 rptWin = window.open( "","rpt","witdh="+w+",height="+h+",top="+ypos+",left="+xpos+",status=yes,scrollbars=yes");
	 rptWin.document.open();
	 rptWin.document.write(rptContent);
	 rptWin.document.close();
	 rptWin.print();
	 rptWin.close();
}

function printList()
{
	var contentData = $(".listTop").html();
	$(".content_sime_list").each(function(){
		 contentData += "<div style='clear:both;'></div>";
		 contentData += $(this).html();
	});

	$(".print").html("<div style='clear:both;margin-top:10px;'></div>"+$("#sime_data").html()+"<div style='clear:both;'></div>"+contentData);
}

function printDetailList()
{
	 var sw = screen.witdh;
	 var sh = screen.height;
	 var w = 1200;
	 var h = 800;
	 var xpos = (sw-w)/2;
	 var ypos = (sh-h)/2;
	 var rptHeader = " <html><head><title>印刷 </title> ";
	 var contentData="";

	 $(".prints").each(function(){
		 contentData+="<div style='clear:both;'></div>";
		 contentData+=$(this).html();
	});

	 rptHeader = rptHeader + '<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />';
	 rptHeader + "</head><body>";
	 rptContent0 = "<div style='clear:both;margin-top:10px;'></div>"+$("#sime_data").html()+"<div style='clear:both;'></div>"+contentData;
	 var rptTail = "</body></html>";
	 var rptContent = rptHeader + rptContent0 + rptTail;
	 rptWin = window.open( "","rpt","witdh="+w+",height="+h+",top="+ypos+",left="+xpos+",status=yes,scrollbars=yes");
	 rptWin.document.open();
	 rptWin.document.write(rptContent);
	 rptWin.document.close();
	 rptWin.print();
	 rptWin.close();
}