//submit
$(function(){
  $("form#common").submit( function () {
		/* ■削除 */
		if($('div').hasClass('delete_div') == true){
			if(confirm( checkDelete )){ //削除します。宜しいですか？
				return true;
			} else {
				return false;
			}
		}

		/* ■既存にエラーが表示されていたら送信しない */
		if($('div').hasClass('parterror') == true){
			alert( inputdataError ); //入力データにエラーが存在します。
			return false;
		}

		//■■Textチェック
	  	$(":text,textarea").filter(".validate").each(function(){
			//■必須項目のチェック
			$(this).filter(".required").each(function(){
				if($(this).val()==""){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredInput + "</div>"); //入力必須項目です
					$(this).focus();
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■半角数字チェック
			$(this).filter(".R_numeric").each(function(){
				if(!$(this).val().match(/^[-0-9]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumber + "</div>"); //半角数字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".numeric").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[-0-9]*$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumber + "</div>"); //半角数字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■浮動小数チェック
			$(this).filter(".R_float").each(function(){
				if(!$(this).val().match(/^[-0-9]+\.?[0-9]*$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumberDot + "</div>"); //半角数字のみで入力してください（小数点含む）
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".float").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[-0-9]+\.?[0-9]*$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyNumberDot + "</div>"); //半角数字のみで入力してください（小数点含む）
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■時間チェック（半角数字、0000~2359
			$(this).filter(".R_time").each(function(){
				if(!$(this).val().match(/^([0-1]?[0-9]|2[0-3])[0-5][0-9]$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyTimeHHMM + "</div>"); //時間をHHMMの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".time").each(function(){
				if(($(this).val()!="") &&
					(!$(this).val().match(/^([0-1]?[0-9]|2[0-3])[0-5][0-9]$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyTimeHHMM + "</div>"); //時間をHHMMの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".time_HH").each(function(){
				if(($(this).val()!="") &&
					(!$(this).val().match(/^([0-1]?[0-9]|2[0-3])$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyTime24 + "</div>");  //２４時間形式で正しく入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■郵便番号チェック
			$(this).filter(".R_zipcode").each(function(){
				if(!$(this).val().match(/^([0-9])+\-([0-9])+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyZipType + "</div>"); //郵便番号の形式（999-9999）で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".zipcode").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^([0-9])+\-([0-9])+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyZipType + "</div>"); //郵便番号の形式（999-9999）で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■電話番号チェック
			$(this).filter(".R_telno").each(function(){
				if(!$(this).val().match(/^([0-9])+\-([0-9])+\-([0-9])+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyTelType + "</div>"); //電話番号の形式（999-9999-9999）で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".telno").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^([0-9])+\-([0-9])+\-([0-9])+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyTelType + "</div>"); //電話番号の形式（999-9999-9999）で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■メールIDチェック
			$(this).filter(".R_mailid").each(function(){
				if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailId + "</div>"); //メールIDの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".mailid").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailId + "</div>"); //メールIDの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■メールAddressチェック
			$(this).filter(".R_mailadd").each(function(){
				if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailAddr + "</div>"); //メール住所の形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".mailadd").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailAddr + "</div>"); //メール住所の形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//e-mail id check for extra error message div
			$(this).filter(".gmailcheck").each(function(){
				var value=$.trim($(this).val());
				$(this).val(value);
				if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyMailId + "</div>"); //メールIDの形式で入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			//■半角英字チェック
			$(this).filter(".R_alpha").each(function(){
				if(!$(this).val().match(/^[a-z,A-Z]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".alpha").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[a-z,A-Z]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■半角英数チェック
			$(this).filter(".R_alpNumeric").each(function(){
				if(!$(this).val().match(/^[a-zA-Z0-9]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".alpNumeric").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[a-zA-Z0-9]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumeric + "</div>");  //半角英字のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■半角コードチェック
			$(this).filter(".R_code").each(function(){
				if(!$(this).val().match(/^[a-zA-Z0-9\-]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumericHyphen + "</div>"); //半角英数(ハイフン含む）のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".code").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[a-zA-Z0-9\-]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyAlphaNumericHyphen + "</div>"); //半角英数(ハイフン含む）のみで入力してください
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■全角カタカナチェック
			$(this).filter(".R_katakana").each(function(){
				if(!$(this).val().match(/^[ァ-ン]+$/g)){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyDoubleKataka + "</div>"); //全角カタカナのみで入力してください。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
			$(this).filter(".katakana").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/^[ァ-ン]+$/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + onlyDoubleKataka + "</div>"); //全角カタカナのみで入力してください。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});

			//■禁則文字チェック
			$(this).filter(".illegal").each(function(){
				if(($(this).val()!="") &&
				   (!$(this).val().match(/['\"\\|\$+\*={}<>~^`:;.%]+/g))){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + unusableChar + "</div>");  //入力に使用できない文字が含まれています。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
		});

		//■■SELECTのチェック
		$("select").filter(".validate").each(function(){
			if($(this).val()==""){
				$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredTerm + "</div>");  //入力必須項目です。
			} else {
				$('#'+this.id+'_error').html("");
			}
		});

		//■■ラジオボタンのチェック
		$(":radio").filter(".validate").each(function(){
			$(this).filter(".required").each(function(){
				if($("input[name="+$(this).attr("name")+"]:checked").size() == 0){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredTerm + "</div>");  //入力必須項目です。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
		});

		//■■チェックボックスのチェック
		$(":checkbox").filter(".validate").each(function(){
			$(this).filter(".required").each(function(){
				if($(":checkbox:checked",this).size() == 0){
					$('#'+this.id+'_error').html("<div class='errorMsg'>" + requiredTerm + "</div>");  //入力必須項目です。
				} else {
					$('#'+this.id+'_error').html("");
				}
			});
		});

		/* ■エラーが表示されていたら送信しない */
		if($('div').hasClass('errorMsg') == true){
			alert( inputdataError ); //入力データにエラーが存在します。
			return false;
		}
/*
		if(confirm( sendInputdata )){ //以上の内容で送信します。宜しいですか？
			return true;
		} else {
			return false;
		}
*/
	});
});

