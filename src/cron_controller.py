# -*- coding: utf-8 -*-
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import login_required
from google.appengine.api import users
#import json
import logging
from time import sleep
#LOCAL
#from paging import *
from posmodel import *
from reserve_model import *

#from google.appengine.api import mail
from google.appengine.api.labs import taskqueue
from google.appengine.api import urlfetch
import time

#from __main__ import ERROR

#DiretoryInfo
DirName = "cron"
SlashDirName = '/' + DirName + '/'
ctrl_name = DirName + "_controller"



'''
###########################################################################
#-----------threeMonthAgo
AspReceiptTable
AuditSumTable
AuditTable
CalendarData
CustomerHistory
DailyUriage      : 3Month
MonthlyUriage    : 5Year
ProductDailyOutput
ProductDailyUriage
ProductListDaily
ProductMonthlyUriage
ReserveList
ReserveTimeTable
TimecardMonthly

#-----------threeMonthAgo


#-----------sevenYearAgoYearMonth
DailySimeData
ReceiptData
###########################################################################
'''
def delete_daily_data():
    #threeMonthAgo = getCalculateAgoDate(95)
    sixMonthAgo   = getCalculateAgoDate(190)
    #oneYearAgo    = getCalculateAgoDate(370)
    #twoYearAgo    = getCalculateAgoDate(740)
    threeYearAgo  = getCalculateAgoDate(1100)
    fiveYearAgo   = getCalculateAgoDate(1850)
    sevenYearAgo  = getCalculateAgoDate(2560)
    #--
    sixMonthAgoYmd  = sixMonthAgo.strftime("%Y%m%d")
    threeYearAgoYmd = threeYearAgo.strftime("%Y%m%d")
    sevenYearAgoYmd = sevenYearAgo.strftime("%Y%m%d")
    threeYearAgoYm = threeYearAgo.strftime("%Y%m")
    fiveYearAgoYm  = fiveYearAgo.strftime("%Y%m")
    fiveYearAgoYY  = fiveYearAgo.strftime("%Y")
    #--
    db.delete( AuditTable.all().filter(     "ymd < ",  threeYearAgoYmd) )
    db.delete( AuditSumTable.all().filter("month < ",  threeYearAgoYm) )
    #--
    db.delete( CashHistory.all().filter("ymd < ", threeYearAgoYmd) )
    db.delete( CashYearly.all().filter("year < ", fiveYearAgoYY) )
    #--
    db.delete( DailyUriage.all().filter(     "ymd < ", threeYearAgoYmd) )
    db.delete( MonthlyUriage.all().filter("yyyymm < ", fiveYearAgoYm) )
    #--
    db.delete( AspReceiptTable.all().filter(   "ymd < ",   sevenYearAgoYmd) )
    db.delete( DailySimeData.all().filter(     "ymd < ",   sevenYearAgoYmd) )
    db.delete( DailySimeDataMulti.all().filter("ymd < ",   sevenYearAgoYmd) )
    db.delete( ReceiptData.all().filter("service_date < ", sevenYearAgoYmd) )
    #--
    db.delete( SiireDaily.all().filter(  "ymd < ",    threeYearAgoYm) )
    db.delete( SiireMonthly.all().filter("yyyymm < ", fiveYearAgoYm) )
    db.delete( SiireYearly.all().filter( "year < ",   fiveYearAgoYY) )
    #--
    db.delete( TimecardMonthly.all().filter("year_month < ", threeYearAgoYm) )

    '''
    db.delete( CalendarData.all().filter("year_month < ", sixMonthAgoYm) )
    db.delete( CustomerHistory.all().filter("ymd < ", threeMonthAgoYmd) )
    db.delete( CustomerInfo.all().filter("ymd < ", threeMonthAgoYmd) )
    db.delete( MessageTable.all().filter("ymd < ", threeMonthAgoYmd) )
    '''
    #logging.info("cron_controller::delete_daily_data  ProductDailyUriage ymd=%s" %ymd )


def task_queue_receipt(shop_id, start_date):
    result_list = []
    key_name = shop_id + start_date

    query_data = ReceiptData.all().filter('shop_id =', shop_id).filter('service_date =', start_date)
    for item in query_data:
        tmp = []
        tmp.append("1")
        tmp.append(item)
        tmp.append( zip(item.st_product_name, item.it_unit_price, item.it_quantity, item.it_sum, item.it_tax_type))
        tmp.append( item.outer_tax + item.inner_tax )
        result_list.append(tmp)


def delete_monthly_data(ym=None):
    if not ym:
        oneYearAgo = getCalculateAgoDate(400)
        ym = oneYearAgo.strftime("%Y%m")
    #logging.debug("cron_controller::delete_monthly_data  DailyUriage ym=%s" %ym )
    #db.delete( DailyUriage.all().filter("ymd > ", "%s00"%ym).filter("ymd < ", "%s99"%ym) )


'''
'''
def timecard_close(shop_id=None, year_month=None):
    if  not year_month or not shop_id:
        logging.error("cron_controller::timecard_close::  not defined month and shop" )
        return

    #logging.info("cron_controller::timecard_close::  month=%s,  shop_id=%s" % (year_month, shop_id))
    query = TimecardMonthly.all().filter("shop_id =", shop_id).filter("year_month =", year_month)
    #
    sum_labor = 0
    for item in query:
        #obj_staff = StaffUser.get_by_key_name( "%s%s"%(item.shop_id, item.staff_id) )
        tmp = [item.staff_id, item.staff_name, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        #if item.sum_work_day > 0: continue
        #
        for i in range(31):
            if item.real_time[i] == 0: continue
            tmp[2] += 1
            tmp[3] += (int(item.real_time[i]/100)*60 + item.real_time[i]%100)
            tmp[4] += (int(item.time1_cnt[i]/100)*60 + item.time1_cnt[i]%100)
            tmp[8] += (int(item.time2_cnt[i]/100)*60 + item.time2_cnt[i]%100)  #深夜
            tmp[6] += (int(item.time3_cnt[i]/100)*60 + item.time3_cnt[i]%100)
            tmp[11] += item.add_tr[i]
            tmp[12] += item.add_fee[i]
        #
        if item.staff_kbn == 1:
            tmp[13] = item.monthly_sal
        else: #
            tmp[5] = int(tmp[4] * item.time_sal_1 / 60)
            tmp[7] = int(tmp[6] * item.time_sal_2 / 60)  #深夜
            tmp[9] = int(tmp[8] * item.time_sal_3 / 60)
            #logging.debug("cron_controller::shop_id=%s, year_month=%s::staff_id=%s, A8=%d, A9=%d" %(shop_id, year_month, item.staff_id,tmp[8],obj_staff.time_sal_3  ))
        if item.daily_tr > 0:
            tmp[10] = tmp[2] * item.daily_tr
        else:
            tmp[10] = item.monthly_tr
        #
        tmp[3] = int(tmp[3]/60)*100 + tmp[3]%60
        tmp[4] = int(tmp[4]/60)*100 + tmp[4]%60
        tmp[6] = int(tmp[6]/60)*100 + tmp[6]%60
        tmp[8] = int(tmp[8]/60)*100 + tmp[8]%60
        #以下の項目は締め後に登録される。
        item.sum_work_day   = tmp[2]  #勤務した日数
        item.sum_real_time  = tmp[3]  #実勤務時間
        item.sum_time1_cnt  = tmp[4]  #時給①時間数合計  アルバイトのみ
        item.sum_time1_kin  = tmp[5]  #時給①金額合計
        item.sum_time2_cnt  = tmp[8]  #時給②時間数合計
        item.sum_time2_kin  = tmp[9]  #時給②金額合計
        item.sum_time3_cnt  = tmp[6]  #時給③時間数合計
        item.sum_time3_kin  = tmp[7]  #時給③金額合計
        item.sum_daily_tr   = tmp[10] #日払い交通費　　　アルバイトのみ
        item.sum_add_tr     = tmp[11] #追加交通費
        item.sum_add_fee   = tmp[12] #追加手当
        item.monthly_sal    = tmp[13] #月給
        item.put()
        sum_labor += (tmp[5]  + tmp[7]  + tmp[9] + tmp[10] + tmp[11] + tmp[12]  + tmp[13])
    #--- MonthlyUriage update
    obj_shop = get_mem_shop(shop_id)
    if obj_shop.it_shop_option and obj_shop.it_shop_option[1]:#勤怠システムから取得
        obj_mon = MonthlyUriage.get_by_key_name("%s%s"%(shop_id, year_month))
        if obj_mon:
            obj_mon.it_labor = [sum_labor,0,0,0,0]
            obj_mon.put()

'''
'''
def siire_close(shop_id=None, year_month=None):
    if  not year_month or not shop_id:
        logging.error("cron_controller::siire_close::  not defined month and shop" )
        return

    #logging.info("cron_controller::siire_close::  month=%s,  shop_id=%s" % (year_month, shop_id))
    idx = int(year_month[4:6]) - 1
    siire_food   = 0
    siire_drink  = 0
    siire_etc    = 0
    siire_broken = 0
    query = SiireYearly.all().filter("shop_id =", shop_id).filter("year =", year_month[0:4])
    for item in query:
        siire_food   += item.it_food[idx]
        siire_drink  += item.it_drink[idx]
        siire_etc    += item.it_etc[idx]
        if item.it_broken:
            siire_broken += item.it_broken[idx]
    #--- MonthlyUriage update
    obj_shop = get_mem_shop(shop_id)
    if obj_shop.it_shop_option and obj_shop.it_shop_option[0]:#仕入システムから取得
        obj_mon = MonthlyUriage.get_by_key_name("%s%s"%(shop_id, year_month))
        if obj_mon:
            obj_mon.it_siire = [siire_food, siire_drink, siire_etc, siire_broken]
            obj_mon.put()

'''
'''
class TimecardCloseClass(Helper):
    def post(self):
        shop_id    = self.request.get('shop_id')
        year_month = self.request.get('year_month')
        timecard_close(shop_id, year_month)
        return

'''
'''
class SiireCloseClass(Helper):
    def post(self):
        shop_id    = self.request.get('shop_id')
        year_month = self.request.get('year_month')
        siire_close(shop_id, year_month)
        return


class TimecardCloseClassQueue(Helper):
    def get(self):
        d = getJST()
        year  = d.year
        month = d.month - 2
        if month < 1:
            year  = year - 1
            month = month + 12
        year_month = "%04d%02d"%(year, month) #２ヶ月前
        #
        query = ShopInfo().all()
        for shop in query:
            #logging.debug("cron_controller::CronReceiptExpand:: receipt_no="  + receipt.receipt_no)
            taskqueue.add(url='/cron/monthly', params={'shop_id': shop.shop_id, 'year_month':year_month})


'''
class CronTimely(Helper):
    def get(self):
        logging.debug("cron_controller::CronDaily:: called" )
        message = mail.EmailMessage(sender="berutio@gmail.com",
                                    subject="CronDaily Test")
        message.to = "berutio@gmail.com"
        message.body = "This is CronDaily TEST"
        #message.send()

        #func_receipt_multi()
'''


'''
'''
class CronDaily(Helper):
    def get(self):
        #logging.debug("cron_controller::CronDaily:: called" )
        #予約単価適用
        self.price_reserved_exec()
        #
        #query = ShopFlagInfo().all()
        #for shop in query:
        #    taskqueue.add(url='/cron/timecardclose', params={'shop_id': shop.shop_id, 'year_month':year_month})
        # 仕入れ、勤怠管理は最大２ヶ月前まで修正可能。
        #   但し、毎月初に先月の決算をしたため、先月分が修正された場合、再決算を行う。
        #
        d = getJST()
        year  = d.year
        month = d.month - 1
        if month < 1:
            year  = year - 1
            month = month + 12
        year_month = "%04d%02d"%(year, month) #１ヶ月前
        #
        query = ShopFlagInfo.all()
        for shop_flag in query:
            if shop_flag.siire_update_flag:
                taskqueue.add(url='/cron/siireclose', params={'shop_id': shop_flag.shop_id, 'year_month':year_month})
            if shop_flag.timecard_update_flag:
                taskqueue.add(url='/cron/timecardclose', params={'shop_id': shop_flag.shop_id, 'year_month':year_month})
                #shop_flag.timecard_update_flag = 0
            #---
            if shop_flag.siire_update_flag or shop_flag.timecard_update_flag:
                shop_flag.siire_update_flag = 0
                shop_flag.timecard_update_flag = 0
                shop_flag.put()
        #日別データ削除
        delete_daily_data()
        return

    def price_reserved_exec(self):
        '''
        #reserved_price_date
        #
        '''
        now = getJST()
        current_date = now.strftime("%Y/%m/%d")

        query = ShopInfo().all().filter("reserved_price_date !=", "").filter("reserved_price_date <=", current_date)
        for shopdata in query:
            subquery = ProductTable().all().filter("shop_id =", shopdata.shop_id).filter("price_reserved >", 0)
            for item in subquery:
                item.price_unit = item.price_reserved
                item.price_reserved = 0
                item.put()
            shopdata.reserved_price_date = ""
            shopdata.put()
            del_mem_shop(shopdata.shop_id)


'''
class CronMonthlyTemp(Helper):
    def get(self):
        logging.debug("cron_controller::CronMonthly:: called" )
        #Calendar Create 4 Month Later
        #TimeCard締め⇒締め後は修正不可--> 1ヶ月前のデータ
        #毎月２日 08:00に実施
        #TenpoTimeListのLogicをCopy
        d = getJST()
        year  = d.year
        month = d.month - 1
        if month < 1:
            year  = year - 1
            month = month + 12
        year_month = "%04d%02d"%(year, month) #２ヶ月前
        #
        query = ShopInfo().all()
        for shop in query:
            taskqueue.add(url='/cron/timecardclose', params={'shop_id': shop.shop_id, 'year_month':year_month})
            taskqueue.add(url='/cron/siireclose', params={'shop_id': shop.shop_id, 'year_month':year_month})
'''

'''
'''
class CronMonthly(Helper):
    def get(self):
        logging.debug("cron_controller::CronMonthly:: called" )
        urlfetch.set_default_fetch_deadline(60)
        '''
        #TimeCard締め⇒締め後は修正不可--> 1ヶ月前のデータ
        #毎月２日 08:00に実施
        #TenpoTimeListのLogicをCopy
        '''
        d = getJST()
        year  = d.year
        month = d.month - 1
        if month < 1:
            year  = year - 1
            month = month + 12
        year_month = "%04d%02d"%(year, month) #２ヶ月前
        #
        query = ShopInfo().all()
        for shop in query:
            if shop.flag_del: continue
            taskqueue.add(url='/cron/timecardclose', params={'shop_id': shop.shop_id, 'year_month':year_month})
            taskqueue.add(url='/cron/siireclose', params={'shop_id': shop.shop_id, 'year_month':year_month})
            sleep(1)
        #----
        #Calendar Create 4 Month Later
        #self.make_calendar_4()  --> reserve_controllで行うこと。
        #
        logging.debug("cron_controller::make_aspreceipt::  month=%s  " % (year_month))
        self.make_aspreceipt()
        #月別データ削除
        logging.debug("cron_controller::delete_monthly_data::  month=%s  " % (year_month))
        delete_monthly_data()
        #TimeCard CLOSE
        #timecard_close()
        return


    def make_aspreceipt(self):
        '''請求書作成
        '''
        d = getJST()
        year_month = "%04d%02d"%(d.year, d.month)

        obj_tax = TaxTable.get_by_key_name("1")

        serial_no = 0
        query = ShopInfo.all()
        for obj_shop in query:
            if obj_shop.asp_period > 0 and (not obj_shop.asp_last_month or obj_shop.asp_last_month <= year_month):
                serial_no += 1
                amount    = obj_shop.asp_period * obj_shop.asp_month_cost
                from_date = d + datetime.timedelta(days=31)
                to_date   = d + datetime.timedelta(days=obj_shop.asp_period*31)
                ret_obj = AspReceiptTable.insert_or_fail(
                                shop_id    = obj_shop.shop_id,
                                seikyu_no  = "ASP%04d%02d%02d%04d"%(d.year, d.month, d.day, serial_no),
                                amount     = amount,
                                tax        = int(amount * obj_tax.tax_pct / 100),
                                from_month = "%04d%02d"%(from_date.year, from_date.month),
                                to_month   = "%04d%02d"%(to_date.year, to_date.month)
                           )
                if ret_obj:
                    obj_shop.asp_last_month = "%04d%02d"%(to_date.year, to_date.month)
                    obj_shop.asp_hurikomi   = 0
                    obj_shop.put()

    def make_calendar_4(self):
        '''
        #４ヶ月間のカレンダー作成
        #「予約・お客」ASPを使う店舗のみ
        '''
        d = getJST()
        year  = d.year
        month = d.month + 4
        if month > 12:
            year  = year + 1
            month = month - 12
        year_month = "%04d%02d"%(year, month)

        query = ShopInfo.all()
        for obj_shop in query:
            if obj_shop.it_setup[2] == 0: continue  #ASP設定 0:POSクラウド, 1:勤怠・人件費, 2:予約・お客, 3:シフト, 4:アンケート
            cal_data = CalendarData.get_by_key_name(obj_shop.shop_id + year_month)
            if not cal_data:
                cal_data = CalendarData.initialize(
                                shop_id        = obj_shop.shop_id,
                                year_month     = year_month,
                                time_open      = obj_shop.time_open,
                                time_close     = obj_shop.time_close,
                                rsv_start_time = obj_shop.rsv_start_time,
                                rsv_end_time   = obj_shop.rsv_end_time,
                                rsv_max_man    = obj_shop.rsv_max_man
                            )

'''
class TestClass(Helper):
    def get(self):
        ymd = self.request.get('ymd')
        if ymd:
            pass
            #delete_history_data(ymd)




def func_shop_section(obj_shop):
    query_data = SectionTable.all().filter('shop_id =', obj_shop.shop_id)
    data = []
    for section in query_data:
        data.append(section.food_drink)
    obj_shop.it_section_kbn = data
    obj_shop.put()
    return


class CronReceiptExpandTest(Helper):
    def get(self):
        shops  = ShopInfo.all()
        for shop in shops:
            if not shop.it_section_kbn:
                func_shop_section(shop)

'''


app = webapp.WSGIApplication([
    ("/cron/daily",    CronDaily),
    ("/cron/monthly",  CronMonthly),
    #("/cron/monthlytemp",  CronMonthlyTemp),
    #("/cron/timely",   CronTimely),
    #,
    ("/cron/timecardclose", TimecardCloseClass),
    ("/cron/siireclose",    SiireCloseClass),
    #("/cron/test",  TestClass),
    ], debug=True)
