# -*- coding: utf-8 -*-
import os
import logging
import datetime
import calendar

from datetime import date
from calendar import weekday

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import memcache

def WriteAjaxResponse(handler, writer):
    handler.response.headers['Access-Control-Allow-Origin'] = '*'
    handler.response.headers['Access-Control-Allow-Headers'] = '*'
    handler.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS'
    handler.response.headers['Content-Type'] = 'application/json'
    writer()


def func_get_fromto_month(self, obj_shop):
    start_date = self.request.get('startDate').replace("/", "")
    if not start_date:
        now = getJST() #
        '''
        if now.month == 12:
            preyear = datetime.date(now.year, 1, 1)
        else:
            preyear = datetime.date(now.year-1, now.month+1, 1)
        start_date = preyear.strftime("%Y%m")
        '''
        start_date = now.strftime("%Y%m")
    #ForTEST
    #    if obj_shop.start_ymd[0:6] > start_date:
    #        start_date = obj_shop.start_ymd[0:6]
    end_date = self.request.get('endDate').replace("/", "")
    if not end_date:
        now = getJST()
        end_date = now.strftime("%Y%m")
    return start_date, end_date


def func_get_common_shopinfo(self, loginData):
    shop_id = self.request.get('shop_id')
    if not shop_id:
        shop_id = self.request.get('sel_shop_id')
        if not shop_id:
            shop_id  = loginData['shop_user'].shop_id
    obj_shop = get_mem_shop(shop_id)
    shops=[]
    '''
    if loginData['shop_user'].role_flag < 2: #POS、店長（店舗指定）
        shops.append(loginData['shop_info'])
    else:
        shops = loginData['shop_user'].company_key.shopinfo_set
        if shop_id != loginData['shop_user'].shop_id:
            loginData['shop_user'].shop_id = shop_id
            loginData['shop_user'].put()
            userKey = "user/" +  loginData['shop_user'].email_id
            memcache.Client().delete(userKey)
    '''
    return shop_id, obj_shop, shops


class UserException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def func_error_display(self, param):
    template_vals = {
            'title':"エラー内容表示",
            'error':param,
        }
    logging.error('Exception ERROR Message = ' + param)
    self.helpDispatch('init', 'errordisp.html', template_vals)
    return

def getCalculateAgoDate(para_day=0):
    time = datetime.datetime.now()
    return time - datetime.timedelta(days = para_day)

def getJST(time = None):
    if not time:
        time = datetime.datetime.now()
    return time + datetime.timedelta(hours=9)

def getYearList(obj_shop):
    year_list = []
    start_year = int(obj_shop.start_ymd[:4])
    cur_date = datetime.datetime.now()
    for idx in range(7):
        year_list.append(u"%04d"%(cur_date.year - idx))
        if start_year == cur_date.year - idx: break
    return year_list


def get_mem_company(company_id):
    companyKey =  "company/" + company_id
    data = memcache.Client().get(companyKey)
    if data == None:
        data = CompanyInfo.get_by_key_name(company_id)
        if data == None:
            logging.error('posmodel::get_mem_company::not found company=' + company_id)
            return None
        memcache.Client().set(key = companyKey, value = data, time = 300)
    return data

def del_mem_company(company_id):
    companyKey =  "company/" + company_id
    memcache.Client().delete(companyKey)
    return

def get_mem_user(user):
    '''memcache get_mem_user
    @param user:
    @return: ShopUser Record
    '''
    emailString  = user.email()
    email_id = emailString[:emailString.find("@")]

    userKey =  "user/" + email_id
    data = memcache.Client().get( userKey )
    if data == None:
        data = ShopUser.get_by_key_name(email_id)
        if data == None:
            logging.debug('posmodel::get_mem_user::not found user=' + email_id)
            return None
        memcache.Client().set(key = userKey, value = data, time = 300)
    return data

def get_mem_shop(shop_id):
    '''memcache get_mem_shop
    @param shop_id: company_id(6桁) + SEQ(2桁)
    @return: ShopInfo Record
    '''
    mem_shop_key =  "shop/" + shop_id
    data = memcache.Client().get(mem_shop_key)
    if data == None:
        data = ShopInfo.get_by_key_name(shop_id)
        if data == None:
            logging.error('posmodel::get_mem_shop::not found shop=' + shop_id)
            return None
        memcache.Client().set(key = mem_shop_key, value = data, time = 300)
    return data

def del_mem_shop(shop_id):
    shopKey =  "shop/" + shop_id
    memcache.Client().delete(shopKey)
    return

'''
'''
def get_mem_section(section_key):
    '''memcache get_mem_section
    @param section_key: shop_id + section_id
    @return: ShopInfo Record
    '''
    mem_section_key =  "section/" + section_key
    data = memcache.Client().get(mem_section_key)
    if data == None:
        data = SectionTable.get_by_key_name(section_key)
        if data == None:
            logging.error('posmodel::get_mem_section::not found section=' + mem_section_key)
            return None
        memcache.Client().set(key = mem_section_key, value = data, time = 60)
    return data

def get_mem_product(product_key):
    '''memcache get_mem_section
    @param product_key: shop_id + product_id
    @return: ShopInfo Record
    '''
    mem_product_key =  "product/" + product_key
    data = memcache.Client().get(mem_product_key)
    if data == None:
        data = PointTable.get_by_key_name(product_key)
        if data == None:
            logging.error('posmodel::get_mem_product::not found product=' + mem_product_key)
            return None
        memcache.Client().set(key = mem_product_key, value = data, time = 60)
    return data

def del_mem_product(product_key):
    mem_product_key =  "product/" + product_key
    memcache.Client().delete(mem_product_key)
    return

###################################################
# Common Module
###################################################
class ApplicationHelper():
    def render(self, group, template):
        return os.path.join(os.path.dirname(__file__), 'views/' + group, template) # テンプレートのOSファイル名を返す

class Helper(webapp.RequestHandler, ApplicationHelper):
    def helpDispatch(self, template_group, template_name, vals): # ディスパッチ用のヘルパーメソッド
        self.response.out.write(template.render(\
            self.render(template_group, template_name), vals)) # 指定されたテンプレートをレンダリングするメソッド

    def logincheck(self, flag=0):
        '''logincheck
        @param :
        @return: loginData
        '''
        logging.getLogger().setLevel(logging.DEBUG)
        user = users.get_current_user()
        if not user:
            template_vals = {
                    'title':"ログイン失敗",
                    'loginData':None,
                }
            logging.debug('login error!!! ')
            if flag==0:
                self.helpDispatch('init', 'unlogined.html', template_vals)
            return

        loginData = {'url':users.create_logout_url("/"),
                     'user':user,
                     'shop_user':None,
                     'shop_info':None,
                     'url_linktext':'Logout'}

        objShopUser = get_mem_user(user)
        if not objShopUser:
            template_vals = {
                    'title':"未登録ユーザ",
                    'loginData':loginData,
                }
            if flag==0:
                logging.debug('unregistered user!!! user=' + user.email())
                self.helpDispatch('init', 'unregistered.html', template_vals)
            else:
                if flag==1: #AJAX
                    logging.debug('unregistered user ajax!!! user=' + user.email())
            return

        loginData['shop_user'] = objShopUser
        loginData['shop_info'] = get_mem_shop(objShopUser.shop_id)

        return loginData


'''
###########################################################################
# データの保存期間
#  1. DailyData   --> 3Month
#  2. MonthlyData --> 5Years
#
# JoinPOS テーブル定義
#  2. CompanyInfo
# 10. StaffUser
###########################################################################
'''

'''
#2
'''
class CompanyInfo(db.Model):
    company_id = db.StringProperty()   #KEYNAME(6degit serialno)
    kname      = db.StringProperty(indexed=False)    #ShopName
    zipcode    = db.StringProperty(indexed=False)    #郵便番号
    address1   = db.StringProperty(indexed=False)    #住所１
    address2   = db.StringProperty(indexed=False)    #住所２
    address3   = db.StringProperty(indexed=False)    #住所3
    telno        = db.StringProperty(indexed=False)    #電話番号
    faxno        = db.StringProperty(indexed=False)    #FAX番号
    lastno_shop  = db.IntegerProperty(indexed=False)   #店舗最後番号 (店舗数が１個しかない会社の区分にも使われる）
    admin_id     = db.StringProperty(indexed=False)   #管理者
    #color_tab    = db.StringListProperty(indexed=False) #
    start_ymd    = db.StringProperty(indexed=False)   #使用開始年月(YYYYMMDD)
    agency_no    = db.IntegerProperty()   #仕入先最後番号

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["company_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.lastno_shop  = 0
        entity.lastno_siire = 0
        d = getJST()
        entity.start_ymd = "%04d%02d%02d"%(d.year, d.month, d.day) #
        entity.color_menu = ['#dcdcdc', '#a8baff', '#fffaa8', '#ffa8ae', '#b3ffa8']
        entity.put()
        '''
        #점포관리자 ID는 원하는 경우에만 생성하도록하고
        ShopUser().insert_or_fail(
                shop_id     = entity.company_id + "01",
                email_id    = entity.admin_id,
                role_flag   = 9
              )
        '''
        return entity

'''
#3
'''
class ShopInfo(db.Model):
    shop_id      = db.StringProperty()   #8degit
    kname        = db.StringProperty(indexed=False)    #ShopName
    zipcode      = db.StringProperty(indexed=False)    #郵便番号
    address1     = db.StringProperty(indexed=False)    #住所１
    address2     = db.StringProperty(indexed=False)    #住所２
    address3     = db.StringProperty(indexed=False)    #住所3
    telno        = db.StringProperty(indexed=False)    #電話番号
    faxno        = db.StringProperty(indexed=False)    #FAX番号
    admin_id     = db.StringProperty(indexed=False)    #管理者
    #
    update_date  = db.StringProperty(indexed=False) #入力日

    @classmethod
    def insert_or_fail(cls, **kwds):
        obj_comp = get_mem_company(kwds["company_id"])
        if not obj_comp: return
        #Tenpo番号自動UP
        obj_comp.lastno_shop += 1
        obj_comp.put()
        del_mem_company(obj_comp.company_id)

        key_name = obj_comp.company_id + "%02d"%(obj_comp.lastno_shop)
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.shop_id  = key_name
        entity.put()

        ''' #ShopUser追加
        '''
        if 'role_flag' not in kwds:
            role_flag = 1
        else:
            role_flag = kwds["role_flag"]
        if entity.admin_id:
            ShopUser.insert_or_fail(
                            shop_id     = entity.shop_id,
                            email_id    = entity.admin_id,
                            role_flag   = role_flag
                          )
        return entity


'''
#5. 部門マスタ
'''
class SectionTable(db.Model):
    shop_id = db.StringProperty()  #KEY 8degit
    sect_id = db.StringProperty()  #KEY 3degit 部門コード
    kname   = db.StringProperty(indexed=False) #部門名（漢字）

    @classmethod
    def insert_or_fail(cls, **kwds):
        obj_shop = get_mem_shop(kwds["shop_id"])
        sect_id = "%02d"%obj_shop.lastno_sect
        key_name = kwds["shop_id"] + sect_id
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            #logging.error('posmodel::PointTable::already exist...then,+1 =' + key_name)
            for last_no in range(obj_shop.lastno_sect + 1, 99):
                obj_shop.lastno_sect += 1
                sect_id = "%02d"%obj_shop.lastno_sect
                key_name = kwds["shop_id"] + sect_id
                entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
                if not entity:
                    break
            if entity:
                logging.error('posmodel::SectionTable::can not append, already exist=' + key_name)
                return None

        entity = cls(key_name=key_name, **kwds)
        entity.sect_id = sect_id
        entity.put()

        obj_shop.lastno_sect += 1
        obj_shop.put()
        #logging.info('posmodel::SectionTable:: obj_shop.lastno_sect=%d'%(obj_shop.lastno_sect))
        del_mem_shop(kwds["shop_id"])
        obj_shop = get_mem_shop(kwds["shop_id"])
        #logging.info('posmodel::SectionTable:: obj_shop.lastno_sect=%d'%(obj_shop.lastno_sect))
        return entity

'''
#7. DEVICE マスタ
１．当分は部門を考えなく、DEVICEのみを管理する。
'''
class PointTable(db.Model):
    shop_id   = db.StringProperty()  #
    sect_id   = db.StringProperty()  # 3degit 部門コード(MultiGW번호)
    point_id = db.StringProperty()  # CHIP_NO or 디바이스번호(MultiGW번호 3자리 + RTD번호 2자리)
    kname     = db.StringProperty(indexed=False)  # ディバイス名
    limit_up  = db.FloatProperty(indexed=False)   # 上限
    limit_dn  = db.FloatProperty(indexed=False)   # 下限
    location  = db.StringProperty(indexed=False)  # 設置場所
    comment   = db.StringProperty(indexed=False)  # COMMENT
    start_ymd = db.StringProperty(indexed=False)  # 使用開始日
    auto_update_date = db.DateTimeProperty(auto_now=True, indexed=False) #更新日

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["uid"] + kwds["uid"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            #logging.debug('posmodel::PointTable::can not append, already exist=' + key_name)
            return entity
        entity = cls(key_name=key_name, **kwds)
        if not entity.sect_id:  entity.sect_id  = "001"
        if not entity.kname:    entity.kname    = ""
        if not entity.limit_up: entity.limit_up = 50
        if not entity.limit_dn: entity.limit_dn = 0
        if not entity.location: entity.location = ""
        if not entity.comment:  entity.comment  = ""
        if not entity.start_ymd:
            d = getJST()
            entity.start_ymd = "%04d%02d%02d"%(d.year, d.month, d.day) #
        entity.put()
        return entity


import random #For Test
'''
#History....
１．当分は部門を考えなく、履歴のみを管理する。
1Record =  two most obvious limits are the 1MB maximum entity size and 20000 index entries.
INTERVALはMIN 10秒にする。
'''
class TloggerHistory(db.Model):
    shop_id   = db.StringProperty()  #
    point_id = db.StringProperty()  # PLUコード KEY1
    ymd       = db.StringProperty()  # YYMMDD KEY2
    ft_data   = db.ListProperty(float, indexed=False) #10초별온도값: Float List	360 * 24 = 8640
    it_time   = db.ListProperty(int, indexed=False)   #時分秒        : INT List   360 * 24 = 8640
    ft_alarm_data = db.ListProperty(float, indexed=False)
    it_alarm_time = db.ListProperty(int, indexed=False)   #時分秒6桁
    last_time = db.IntegerProperty(indexed=False)  #最終受付データの時刻：この時間以降のデータを受け取る

    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = "%s%s%s" % (kwds["shop_id"], kwds["point_id"], kwds["ymd"])
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.put()
        return entity

'''
#9. ログインユーザ
'''
class ShopUser(db.Model):
    email_id    = db.StringProperty()   #KEY
    shop_id     = db.StringProperty()   #
    role_flag   = db.IntegerProperty(indexed=False)  #0:店舗Staff, 1:店長, 2:店長＋他店照会, 9:本部
    disabl_edit = db.IntegerProperty(indexed=False)  #0 or Null:全ての権限、1:修正権限なし（登録ボタン表示しない）
    @classmethod
    def insert_or_fail(cls, **kwds):
        key_name = kwds["email_id"]
        entity = cls.get_by_key_name(key_name, parent=kwds.get('parent'))
        if entity:
            return None
        entity = cls(key_name=key_name, **kwds)
        entity.disabl_edit = 0
        entity.put()
        return entity
